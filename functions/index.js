const functions = require('firebase-functions');
const admin = require('firebase-admin');
const express = require('express');
const cors = require('cors');
const nodemailer = require('nodemailer');

const app = express();
const fromEmailAddressProd = 'imbisbpc@imbisa.africa';
const domainDev = 'http://localhost:4201';
const domainProd = 'https://imbisa.africa';
const maxFirestoreWrites = 249;

const transport = nodemailer.createTransport({
    host: 'mail.imbisa.africa',
    secure: true,
    requireTLS: true,
    tls: {
        ciphers: 'SSLv3'
    },
    auth: {
        user: 'imbisbpc',
        pass: 'f7P9^j+-j5sF'
    },
    pool: true,
    maxMessages: 500,
    maxConnections: 3
});

app.use(cors({
    origin: [domainDev, domainProd],
    credentials: true
}));

exports.widgets = functions.https.onRequest(app);
admin.initializeApp();

exports.userOnCreate = functions.auth.user().onCreate(user => createUserHandler(user, 'en'));

exports.updateUserEn = functions.firestore
    .document('users/{user}')
    .onUpdate(user => updateUserHandler(user, 'en'));

exports.createArticleEn = functions.firestore
    .document('articles-en/{article}')
    .onCreate(article => createArticleHandler(article, 'en'));

exports.createArticlePt = functions.firestore
    .document('articles-pt/{article}')
    .onCreate(article => createArticleHandler(article, 'pt'));

exports.createPublicArticleEn = functions.firestore
    .document('public-articles-en/{article}')
    .onCreate(article => createPublicArticleHandler(article, 'en'));

exports.createPublicArticlePt = functions.firestore
    .document('public-articles-pt/{article}')
    .onCreate(article => createPublicArticleHandler(article, 'pt'));

exports.createLoginRequestEn = functions.firestore
    .document('login-requests/{loginRequest}')
    .onCreate(loginRequest => createLoginRequestHandler(loginRequest, 'en'));

exports.scheduledEnFunction = functions.pubsub.schedule('every 5 minutes')
    .onRun(context => processMessageQueue('en'));

exports.scheduledPtFunction = functions.pubsub.schedule('every 5 minutes')
    .onRun(context => processMessageQueue('pt'));

app.post('/claims', postHandler);

function createUserHandler(user, locale) {

    return admin.firestore()
        .collection('users')
        .doc(user.uid)
        .set({ email: user.email, visible: true })
        .then(() => sendLoginRequestEmail(user, locale))
        .catch(error => {
            console.log(error);
            return;
        });
}

function createLoginRequestHandler(snapshot, locale) {

    let loginRequest = snapshot.data();
    let subject;
    let body;
    let link = domainProd + '/' + locale + '/login';

    if (locale === 'en') {
        subject = 'Login Request';
        body =
            '<p>An administrator with the following email address, ' + loginRequest.existingUserEmail + ', is asking you to log onto the IMBISA website.</p>' +
            '<p>Instructions:</p>' +
            '<p>Step 1: Navigate to the following login page: <a href="' + link + '">' + link + '</a>.</p>' +
            '<p>Step 2: Click on the LOGIN button. A window popup should display. Choose an account or select "Use another account".</p>' +
            '<p>Step 3: Work through the steps until the window popup disappears.</p>' +
            '<p>An email will be sent to ' + loginRequest.existingUserEmail + ' requesting them to grant you admininstrative rights.</p>' +
            '<p>An email wil be sent to you once the administrator has granted you admininstrative rights.</p>';
    } else {
        subject = '';
        body = '';
    }

    let message = {
        from: fromEmailAddressProd,
        to: loginRequest.newUserEmail,
        subject: subject,
        html: body
    };

    return sendEmails([message]);
}

function updateUserHandler(change, locale) {

    let userBefore = change.before.data();
    let userAfter = change.after.data();

    if (!userBefore.admin && userAfter.admin) {
        return sendAdminRightsEmail(userAfter, locale);
    } else {
        return;
    }
}

function createArticleHandler(snapshot, locale) {

    let article = snapshot.data();
    delete article.id;

    let subject;
    let body;
    let link = domainProd + '/' + locale + '/articles/' + snapshot.id + '/edit';

    if (locale === 'en') {
        subject = 'New Article';
        body = '<a href="' + link + '">' + article.articleHeadline + '</a>';
    } else {
        subject = 'New Article';
        body = '<a href="' + link + '">' + article.articleHeadline + '</a>';
    }

    let mailConfig = {
        from: fromEmailAddressProd,
        subject: subject,
        html: body
    };

    let docRef = admin.firestore().collection('users');
    return docRef
        .where('notifyNewArticle', '==', true)
        .get()
        .then(docSnap => {

            let messages = [];
            docSnap.forEach(doc => {
                let copy = Object.assign({}, mailConfig);
                copy.to = doc.data().email;
                messages.push(copy);
            });

            return sendEmails(messages);
        }).catch(error => {
            console.log(error);
            return;
        });
}

function sendEmails(messages) {
    let promises = [];
    messages.forEach(message => {
        let promise = transport.sendMail(message);
        promises.push(promise);
    });
    if (promises.length) {
        return Promise.all(promises)
            .catch(error => {
                console.log(error);
                return;
            });
    }
    return;
}

function processMessageQueue(locale) {
    let docRef = admin.firestore().collection('subscribers-' + locale);
    return docRef
        .get()
        .then(docSnap => {
            let today = new Date().toISOString();
            let modifyItems = [];
            let emailPromises = [];
            docSnap.forEach(snapshot => {
                let subscriber = Object.assign({ id: snapshot.id }, snapshot.data());
                if (Array.isArray(subscriber.articleQueue)) {
                    subscriber.articleQueue.forEach((articleQueueItem, articleQueueItemIndex) => {
                        if (articleQueueItem.publishDate < today && emailPromises.length <= (maxFirestoreWrites / 2)) {
                            let emailPromise = sendPublicEmail(subscriber.email, articleQueueItem, locale)
                                .then(() => sendPublicEmailSuccess(subscriber, modifyItems, articleQueueItem, articleQueueItemIndex, snapshot))
                                .catch(error => {
                                    console.log('Email not sent to ' + subscriber.email);
                                    console.log(error);
                                    return;
                                });
                            emailPromises.push(emailPromise);
                        }
                    });
                }
            });
            return Promise
                .allSettled(emailPromises)
                .then(() => updateScheduleArticleQueue(modifyItems));
        }).catch(error => {
            console.log(error);
            return;
        });
}

function sendPublicEmailSuccess(subscriber, modifyItems, articleQueueItem, articleQueueItemIndex, snapshot) {
    let result = modifyItems.find(x => x.subscriberId === subscriber.id);
    if (result) {
        let modifyItem = result.articleQueue.find(x => x.articleId === articleQueueItem.articleId);
        result.articleQueue.splice(result.articleQueue.indexOf(modifyItem), 0);
    } else {
        let articleQueueCopy = Object.assign([], subscriber.articleQueue);
        modifyItems.push({
            subscriberId: subscriber.id,
            subscriberRef: snapshot.ref,
            articleQueue: articleQueueCopy.splice(articleQueueItemIndex, 0)
        });
    }
    return;
}

function updateArticleQueueRetry(subscriber, modifyItems) {
    let result = modifyItems.find(x => x.subscriberId === subscriber.id);
    if (result) {
        let modifyItem = result.articleQueue.find(x => x.articleId === articleQueueItem.articleId);
        modifyItem.retry++;
    } else {
        let articleQueueCopy = Object.assign([], subscriber.articleQueue);
        modifyItems.push({
            subscriberId: subscriber.id,
            subscriberRef: snapshot.ref,
            articleQueue: articleQueueCopy,
        });

        let modifyItem = modifyItems[modifyItems.length - 1].articleQueue.find(x => x.articleId === articleQueueItem.articleId);
        modifyItem.retry++;
    }
    return;
}

function updateScheduleArticleQueue(modifyItems) {
    let retryLimit = 2;
    let counter = 1;
    let modifyItemBatch = [];
    let batchIndex = 0;
    let batchPromises = [];

    modifyItems.forEach(modifyItem => {
        if (!Array.isArray(modifyItemBatch[batchIndex])) {
            modifyItemBatch[batchIndex] = [];
        }
        modifyItemBatch[batchIndex].push(modifyItem);
        if (counter === maxFirestoreWrites) {
            batchIndex++;
            counter = 0;
        }
        counter++;
    });

    modifyItemBatch.forEach(modifyItems => {
        let batchPromise = new Promise((resolve, reject) => {
            let firestoreBatch = admin.firestore().batch();
            modifyItems.forEach(modifyItem => {
                firestoreBatch.set(modifyItem.subscriberRef, {
                    articleQueue: []
                }, { merge: true });
            });
            firestoreBatch.commit()
                .then(() => resolve())
                .catch(error => reject(error));
        });
        batchPromises.push(batchPromise);
    });

    if (batchPromises.length) {
        return Promise.all(batchPromises)
            .catch(error => {
                console.log('Article queue items could not be updated.');
                console.log(error);
                return;
            });
    }
    return;
}

function createPublicArticleHandler(snapshot, locale) {
    let docRef = admin.firestore().collection('subscribers-' + locale);
    return docRef
        .get()
        .then(docSnap => {
            let counter = 1;
            let batchInterval = 0;
            let article = Object.assign({ id: snapshot.id }, snapshot.data());
            let today = new Date();
            let modifyItems = [];
            docSnap.forEach(snapshot => {
                let articleQueueCopy = updateArticleQueue(modifyItems, article, batchInterval, snapshot, today);
                if (counter === maxFirestoreWrites) {
                    batchInterval++;
                    console.log('New batch: ' + batchInterval);
                    counter = 0;
                }
                counter += articleQueueCopy.length;
            });
            return updateScheduleArticleQueue(modifyItems);
        }).catch(error => {
            console.log(error);
            return;
        });
}

function updateArticleQueue(modifyItems, article, batchInterval, snapshot, today) {
    let minutes = 10;
    let publishDate = new Date(article.publishDate);
    if (publishDate.getFullYear() === today.getFullYear() && publishDate.getMonth() === today.getMonth() && publishDate.getDay() === today.getDay()) {
        publishDate = today;
    }
    let newPublishDate = new Date(publishDate.getTime() + minutes * 60000 * batchInterval).toISOString();
    let subscriber = Object.assign({ id: snapshot.id }, snapshot.data());
    let articleQueueCopy = Object.assign([], subscriber.articleQueue);
    let modifyItem = articleQueueCopy.find(x => x.articleId === article.id);
    if (modifyItem) {
        modifyItem.publishDate = newPublishDate;
    } else {
        articleQueueCopy.push({
            articleId: article.id,
            articleHeadline: article.articleHeadline,
            authorName: article.authorName,
            region: article.region,
            publishDate: newPublishDate,
            retry: 0
        });
    }
    modifyItems.push({
        subscriberId: subscriber.id,
        subscriberRef: snapshot.ref,
        articleQueue: articleQueueCopy
    });
    return articleQueueCopy;
}

function postHandler(req, res, next) {

    try {
        return admin.auth().verifyIdToken(req.body.token).then((decodedToken) => {
            if (decodedToken.admin) {
                return admin.auth()
                    .setCustomUserClaims(req.body.id, { admin: !!req.body.admin })
                    .then(() => {
                        return admin.firestore()
                            .collection('users')
                            .doc(req.body.id)
                            .set({ admin: !!req.body.admin, visible: !!req.body.visible }, { merge: true })
                            .then(() => {
                                res.status(200).send(null);
                                return;
                            })
                            .catch(error => {
                                console.log(error);
                                next(new Error(error));
                                return;
                            });
                    })
                    .catch(error => {
                        console.log(error);
                        next(new Error(error));
                        return;
                    });
            } else {
                console.log('user does not have admin rights');
                next(new Error('user does not have admin rights'));
                return;
            }
        })
            .catch(error => {
                console.log(error);
                next(new Error(error));
                return;
            });
    } catch (error) {
        console.log(error);
        next(new Error(error));
        return;
    }
}

function sendPublicEmail(subscriberEmail, articleQueueItem, locale) {

    let subject;
    let body;
    let link = domainProd + '/' + locale + '/articles/' + articleQueueItem.articleId + '/view';

    if (locale === 'en') {
        subject = 'New Article - Just Published';
        body =
            '<img style="margin-bottom: 3rem; width: 100%" src="https://imbisa.africa/en/assets/images/email/Imbisa-EmailHeader.png">' +
            '<p>Dear Subscriber,</p>' +
            '<p>a new article titled:</p>' +
            '<div><a style="font-weight: bold; color: #007bff !important; text-decoration: none;" href="' + link + '">' + articleQueueItem.articleHeadline + '</a></div>' +
            '<div style="font-weight: bold;">' + articleQueueItem.authorName + '</div>' +
            '<div style="font-weight: bold;margin-bottom: 1rem;">' + articleQueueItem.region + '</div>' +
            '<p>has just been posted at <a style="font-weight: bold; color: black !important; text-decoration: none;"' +
            '    href="https://imbisa.africa/en/home/start">www.imbisa.africa.</a></p>' +
            '<p>We think you\'ll find it valuable.</p>' +
            '<div>' +
            '  Please feel free to respond to this email address with feedback to help ensure that' +
            '</div>' +
            '<div style="margin-bottom: 1rem;">' +
            '  we always post quality content that is relevant to you and your region.' +
            '</div>' +
            '<p>You are also invited to <a style="font-weight: bold; color: #007bff !important; text-decoration: none;"' +
            '    href="https://imbisa.africa/en/stories">submit your own article</a> by clicking on the link.</p>' +
            '<p>We look forward to hearing from you.</p>' +
            '<p>Happy reading, and God bless.</p>' +
            '<p style="font-weight: bold">The Imbisa Team</p>' +
            '<img src="https://imbisa.africa/en/assets/ImbisaLogo.png" style="width: 25%; margin-bottom: 3rem;" />' +
            '<p>Click <a style="font-weight: bold; color: black !important; text-decoration: none !important;"' +
            '    href="mailto:imbisa@imbisa.com" target="_blank">here</a> to unsubscribe from this mailing list.</p>';
    } else {
        subject = 'Novo artigo - recém publicado';
        body =
            '<img style="margin-bottom: 3rem; width: 100%" src="https://imbisa.africa/en/assets/images/email/Imbisa-EmailHeader.png">' +
            '<p>Estimado assinante,</p>' +
            '<p>Um anovo artigo intitulado:</p>' +
            '<div><a style="font-weight: bold; color: #007bff !important; text-decoration: none;" href="' + link + '">' + articleQueueItem.articleHeadline + '</a></div>' +
            '<div style="font-weight: bold;">' + articleQueueItem.authorName + '</div>' +
            '<div style="font-weight: bold;margin-bottom: 1rem;">' + articleQueueItem.region + '</div>' +
            '<p>acaba de ser publicado na <a style="font-weight: bold; color: black !important; text-decoration: none;"' +
            '    href="https://imbisa.africa/en/home/start">www.imbisa.africa.</a></p>' +
            '<p>Acreditamos que o achará útil.</p>' +
            '<div>' +
            '  Por favor responda a este email com comentários que ajudem a assegurar' +
            '</div>' +
            '<div style="margin-bottom: 1rem;">' +
            '  que publicamos conteúdos de qualidade relevantes para si e para a sua região.' +
            '</div>' +
            '<p>É também convidado <a style="font-weight: bold; color: #007bff !important; text-decoration: none;"' +
            '    href="https://imbisa.africa/en/stories">a apresentar os seus próprios artigos</a>, clicando neste link.</p>' +
            '<p>Esperamos por si.</p>' +
            '<p>Boas leituras e Deus abençoe.</p>' +
            '<p style="font-weight: bold">A Equipa da Imbisa</p>' +
            '<img src="https://imbisa.africa/en/assets/ImbisaLogo.png" style="width: 25%; margin-bottom: 3rem;" />' +
            '<p>Clique <a style="font-weight: bold; color: black !important; text-decoration: none !important;"' +
            '    href="mailto:imbisa@imbisa.com" target="_blank">aqui</a> para deixar de fazer parte desta lista.</p>';
    }

    let message = {
        to: subscriberEmail,
        from: fromEmailAddressProd,
        subject: subject,
        html: body
    };

    return sendEmails([message]);
}

function sendLoginRequestEmail(user, locale) {

    let subject;
    let body;
    let link = domainProd + '/' + locale + '/administrators';

    if (locale === 'en') {
        subject = 'Login Request Completed';
        body =
            '<p>' + user.email + ', is asking you to grant them administrative rights.</p>' +
            '<p>Instructions:</p>' +
            '<p>Step 1: Navigate to the following page: <a href="' + link + '">' + link + '</a>.</p>' +
            '<p>Step 2: Find the email address and set the dropdown under the Admin column to Yes.</p>' +
            '<p>An email will be sent to ' + user.email + ' informing them that you\'ve granted them admininstrative rights.</p>';
    } else {
        subject = '';
        body = '';
    }

    let mailConfig = {
        from: fromEmailAddressProd,
        subject: subject,
        html: body
    };

    let docRef = admin.firestore().collection('login-requests');
    return docRef
        .where('newUserEmail', '==', user.email)
        .get()
        .then(docSnap => {

            let messages = [];
            docSnap.forEach(doc => {
                let copy = Object.assign({}, mailConfig);
                copy.to = doc.data().existingUserEmail;
                messages.push(copy);
            });

            return sendEmails(messages);
        }).catch(error => {
            console.log(error);
            return;
        });
}

function sendAdminRightsEmail(user, locale) {

    let subject;
    let body;
    let link = domainProd + '/' + locale + '/login';

    if (locale === 'en') {
        subject = 'Access Granted';
        body =
            '<p>You have been granted administrative rights.</p>' +
            '<p>Instructions:</p>' +
            '<p>Step 1: Navigate to the following login page: <a href="' + link + '">' + link + '</a>.</p>' +
            '<p>Step 2: Click on the MANAGE button. This will take you to the content manager page.</p>' +
            '<p>Login again if the MANAGE button is disabled.</p>';
    } else {
        subject = '';
        body = '';
    }

    let message = {
        to: user.email,
        from: fromEmailAddressProd,
        subject: subject,
        html: body
    };

    return sendEmails([message]);
}
