using Imbisa.Models;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace Imbisa.Controllers
{
  public class SubscribersController : ApiController
  {
    // POST api/<controller>
    public async Task<IHttpActionResult> Post([FromBody] SubscriberModel subsriber)
    {
      var statusCode = await ConnectionSingleton
        .ExecuteSqlTransactionAsync($"insert into Subscribers values (NEWID(), '{subsriber.Email}');");
      if (statusCode == HttpStatusCode.OK)
      {
        return Ok();
      }
      else if(statusCode == HttpStatusCode.BadRequest)
      {
        return BadRequest();
      }
      else
      {
        return InternalServerError();
      }
    }

    // DELETE api/<controller>/id
    public async Task<IHttpActionResult> Delete([FromBody] SubscriberModel subsriber)
    {
      var statusCode = await ConnectionSingleton
        .ExecuteSqlTransactionAsync($"delete from Subscribers where Email = '{subsriber.Email}';");
      if(statusCode == HttpStatusCode.OK)
      {
        return Ok();
      }
      else
      {
        return InternalServerError();
      }
    }
  }
}
