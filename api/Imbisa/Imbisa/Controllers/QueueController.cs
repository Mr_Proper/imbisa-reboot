using FluentScheduler;
using Imbisa.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Imbisa.Controllers
{
  public class QueueController : ApiController
  {
    // POST api/<controller>
    public async Task<IHttpActionResult> Post([FromBody] QueueModel queue)
    {
      var builder = new StringBuilder();
      List<string> subscribers = await GetSubscribers("select Email from Subscribers");
      subscribers.ForEach(subscriber =>
      {
        builder.Append($"insert into Queue values (NEWID(), '{queue.Locale}', '{queue.Link}', '{queue.Headline}', '{queue.Author}', '{queue.Region}', CONVERT(DATETIME, '{queue.PublishDate.ToString("yyyy-MM-dd HH:mm:ss.fff")}', 102), '{subscriber}', 0);");
      });
      string sql = builder.ToString();
      if (sql.Length > 0)
      {
        var statusCode = await ConnectionSingleton
          .ExecuteSqlTransactionAsync(builder.ToString());
        if (statusCode == HttpStatusCode.OK)
        {
          JobManager.AddJob<EmailJob>(s => s.NonReentrant().ToRunNow());
          return Ok();
        }
        else
        {
          return InternalServerError();
        }
      }
      return Ok();
    }

    private static async Task<List<string>> GetSubscribers(string commandText)
    {
      while (ConnectionSingleton.Reading)
      {
        await Task.Delay(TimeSpan.FromSeconds(ConnectionSingleton.readWaitSeconds));
      }

      ConnectionSingleton.Reading = true;
      var subscribers = new List<string>();

      try
      {
        if (ConnectionSingleton.Connection.State == ConnectionState.Closed)
        {
          await ConnectionSingleton.Connection.OpenAsync();
        }

        var command = ConnectionSingleton.Connection.CreateCommand();
        command.CommandText = commandText;

        using (var reader = await command.ExecuteReaderAsync())
        {
          while (await reader.ReadAsync())
          {
            var email = await reader.GetFieldValueAsync<string>(0);
            subscribers.Add(email);
          }
        }
      }
      catch (Exception ex)
      {
        await ConnectionSingleton.Log(ex);
      }
      finally
      {
        ConnectionSingleton.Reading = false;
      }

      return subscribers;
    }
  }
}
