using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Imbisa.Models
{
  public class QueueModel
  {
    public Guid Id { get; set; }
    public string Locale { get; set; }
    public string Link { get; set; }
    public string Headline { get; set; }
    public string Author { get; set; }
    public string Region { get; set; }
    public DateTime PublishDate { get; set; }
    public string Email { get; set; }
  }
}
