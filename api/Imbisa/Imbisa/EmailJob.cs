using FluentScheduler;
using Imbisa.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Imbisa
{
  public class EmailJob : IJob
  {
    private const int batchSize = 500;
    private const int idlePeriodHours = 1;

    public void Execute()
    {
      int processedQueueCount = GetQueueCount("select COUNT(*) from Queue where Processed = 1;");
      int queueCapacity = batchSize - processedQueueCount;
      if (queueCapacity > 0)
      {
        List<QueueModel> queue = GetQueue($"select top {queueCapacity} Id, Locale, Link, Headline, Author, Region, Email from Queue where PublishDate < CONVERT(DATETIME, '{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")}', 102) and Processed = 0 order by PublishDate;");
        UpdateQueue(queue);
        SendEmails(queue);    
      }
      else if (ConnectionSingleton.RestartBatch < DateTime.Now.AddHours(-idlePeriodHours))
      {
        ConnectionSingleton.RestartBatch = DateTime.Now;
        ConnectionSingleton.ExecuteSqlTransaction($"delete from Queue where Processed = 1;");
      }
    }

    private void SendEmails(List<QueueModel> queue)
    {
      var smtp = new SmtpClient()
      {
        Host = "mail.imbisa.africa",
        EnableSsl = true,
        Port = 25,
        UseDefaultCredentials = true,
        Credentials = new NetworkCredential
        {
          UserName = "imbisbpc",
          Password = "f7P9^j+-j5sF"
        }
      };

      Parallel.ForEach(queue.AsEnumerable(), item =>
      {
        try
        {
          smtp.Send(new MailMessage("imbisbpc@imbisa.africa", item.Email)
          {
            Subject = GetSubject(item.Locale),
            Body = GetBody(item),
            IsBodyHtml = true
          });
        }
        catch (Exception ex)
        {
          Task.Run(() => ConnectionSingleton.Log(ex));
        }
      });
    }

    private string GetSubject(string locale)
    {
      if (locale == "en")
      {
        return "New Article - Just Published";
      }
      else if (locale == "pt")
      {
        return "Novo artigo - recém publicado";
      }
      return string.Empty;
    }

    private string GetBody(QueueModel queue)
    {
      var builder = new StringBuilder();

      if (queue.Locale == "en")
      {
        builder.Append("<img style=\"margin - bottom: 3rem; width: 100 % \" src=\"https://imbisa.africa/en/assets/images/email/Imbisa-EmailHeader.png\">");
        builder.Append("<p>Dear Subscriber,</p>");
        builder.Append("<p>a new article titled:</p>");
        builder.Append($"<div><a style=\"font - weight: bold; color: #007bff !important; text-decoration: none;\" href=\"{queue.Link}\">{queue.Headline }</a></div>");
        builder.Append($"<div style=\"font - weight: bold; \">{queue.Author}</div>'");
        builder.Append($"<div style=\"font - weight: bold; margin - bottom: 1rem; \">{queue.Region}</div>");
        builder.Append("<p>has just been posted at <a style=\"font - weight: bold; color: black!important; text - decoration: none; \" href=\"https://imbisa.africa/en/home/start\">www.imbisa.africa.</a></p>'");
        builder.Append("<p>We think you'll find it valuable.</p>");
        builder.Append("<div>Please feel free to respond to this email address with feedback to help ensure that</div>");
        builder.Append("<div style=\"margin - bottom: 1rem; \">we always post quality content that is relevant to you and your region.</div>");
        builder.Append("<p>You are also invited to <a style=\"font - weight: bold; color: #007bff !important; text-decoration: none;\" href=\"https://imbisa.africa/en/stories\">submit your own article</a> by clicking on the link.</p>");
        builder.Append("<p>We look forward to hearing from you.</p>");
        builder.Append("<p>Happy reading, and God bless.</p>");
        builder.Append("<p style=\"font - weight: bold\">The Imbisa Team</p>");
        builder.Append("<img src=\"https://imbisa.africa/en/assets/ImbisaLogo.png\" style=\"width: 25%; margin-bottom: 3rem;\" />");
        builder.Append("<p>Click <a style=\"font - weight: bold; color: black!important; text - decoration: none!important; \" href=\"mailto: imbisa @imbisa.com\" target=\"_blank\">here</a> to unsubscribe from this mailing list.</p>");
      }
      else if (queue.Locale == "pt")
      {
        builder.Append("<img style=\"margin - bottom: 3rem; width: 100 % \" src=\"https://imbisa.africa/en/assets/images/email/Imbisa-EmailHeader.png\">");
        builder.Append("<p>Estimado assinante,</p>");
        builder.Append("<p>Um anovo artigo intitulado:</p>");
        builder.Append($"<div><a style=\"font - weight: bold; color: #007bff !important; text-decoration: none;\" href=\"{queue.Link}\">{queue.Headline}</a></div>");
        builder.Append($"<div style=\"font - weight: bold; \">{queue.Author}</div>");
        builder.Append($"<div style=\"font - weight: bold; margin - bottom: 1rem; \">{queue.Region}</div>");
        builder.Append("<p>acaba de ser publicado na <a style=\"font - weight: bold; color: black!important; text - decoration: none; \" href=\"https://imbisa.africa/en/home/start\">www.imbisa.africa.</a></p>");
        builder.Append("<p>Acreditamos que o achará útil.</p>");
        builder.Append("<div>Por favor responda a este email com comentários que ajudem a assegurar</div>");
        builder.Append("<div style=\"margin - bottom: 1rem; \">que publicamos conteúdos de qualidade relevantes para si e para a sua região.</div>");
        builder.Append("<p>É também convidado <a style=\"font - weight: bold; color: #007bff !important; text-decoration: none;\" href=\"https://imbisa.africa/en/stories\">a apresentar os seus próprios artigos</a>, clicando neste link.</p>");
        builder.Append("<p>Esperamos por si.</p>");
        builder.Append("<p>Boas leituras e Deus abençoe.</p>");
        builder.Append("<p style=\"font - weight: bold\">A Equipa da Imbisa</p>");
        builder.Append("<img src=\"https://imbisa.africa/en/assets/ImbisaLogo.png\" style=\"width: 25%; margin-bottom: 3rem;\" />");
        builder.Append("<p>Clique <a style=\"font - weight: bold; color: black!important; text - decoration: none!important; \" href=\"mailto: imbisa@imbisa.com\" target=\"_blank\">aqui</a> para deixar de fazer parte desta lista.</p>");
      }
      return builder.ToString();
    }

    private void UpdateQueue(List<QueueModel> queue)
    {
      var builder = new StringBuilder();
      queue.ForEach(item => builder.Append($"update Queue set Processed = 1 where Id = '{item.Id}';"));
      string sql = builder.ToString();
      if(sql.Length > 0)
      {
        ConnectionSingleton.ExecuteSqlTransaction(sql);
      }
    }

    private int GetQueueCount(string commandText)
    {
      while (ConnectionSingleton.Reading)
      {
        Task.Delay(TimeSpan.FromSeconds(ConnectionSingleton.readWaitSeconds));
      }

      ConnectionSingleton.Reading = true;

      try
      {
        if (ConnectionSingleton.Connection.State == ConnectionState.Closed)
        {
          ConnectionSingleton.Connection.Open();
        }

        var command = ConnectionSingleton.Connection.CreateCommand();
        command.CommandText = commandText;

        return (int)command.ExecuteScalar();
      }
      catch (Exception ex)
      {
        Task.Run(() => ConnectionSingleton.Log(ex));
      }
      finally
      {
        ConnectionSingleton.Reading = false;
      }

      return batchSize;
    }

    private List<QueueModel> GetQueue(string commandText)
    {
      while (ConnectionSingleton.Reading)
      {
        Task.Delay(TimeSpan.FromSeconds(ConnectionSingleton.readWaitSeconds));
      }

      ConnectionSingleton.Reading = true;
      var queue = new List<QueueModel>();

      try
      {
        if (ConnectionSingleton.Connection.State == ConnectionState.Closed)
        {
          ConnectionSingleton.Connection.Open();
        }

        var command = ConnectionSingleton.Connection.CreateCommand();
        command.CommandText = commandText;

        using (var reader = command.ExecuteReader())
        {
          while (reader.Read())
          {
            queue.Add(new QueueModel
            {
              Id = reader.GetFieldValue<Guid>(0),
              Locale = reader.GetFieldValue<string>(1),
              Link = reader.GetFieldValue<string>(2),
              Headline = reader.GetFieldValue<string>(3),
              Author = reader.GetFieldValue<string>(4),
              Region = reader.GetFieldValue<string>(5),
              Email = reader.GetFieldValue<string>(6),
            });
          }
        }
      }
      catch (Exception ex)
      {
        Task.Run(() => ConnectionSingleton.Log(ex));
      }
      finally
      {
        ConnectionSingleton.Reading = false;
      }

      return queue;
    }
  }
}
