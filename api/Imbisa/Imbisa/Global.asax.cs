using FluentScheduler;
using System.Web.Http;

namespace Imbisa
{
  public class WebApiApplication : System.Web.HttpApplication
  {
    private const int jobPeriodMinutes = 15;

    protected void Application_Start()
    {
      JobManager.Initialize();
      JobManager.AddJob<EmailJob>(s => s.ToRunEvery(jobPeriodMinutes).Minutes());
      GlobalConfiguration.Configure(WebApiConfig.Register);
    }
  }
}
