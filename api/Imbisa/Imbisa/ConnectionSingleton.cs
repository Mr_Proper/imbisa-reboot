using System;
using System.Data;
using System.Data.SqlServerCe;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace Imbisa
{
  public sealed class ConnectionSingleton
  {
    public const int readWaitSeconds = 30;
    private static volatile SqlCeConnection connection;
    private static object syncRoot = new Object();
    public static volatile bool Reading;
    public static DateTime RestartBatch = DateTime.Now;

    private ConnectionSingleton() { }

    public static SqlCeConnection Connection
    {
      get
      {
        if (connection == null)
        {
          lock (syncRoot)
          {
            if (connection == null)
              connection = new SqlCeConnection("Data Source=" + Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Imbisa.sdf"));
          }
        }

        return connection;
      }
    }

    public static async Task Log(Exception ex)
    {
      string commandText = $"insert into Logs values (NEWID(), '{ex.Message}', '{ex.GetType().Name}', '{ex.StackTrace}', CURRENT_TIMESTAMP);";

      if (Connection.State == ConnectionState.Closed)
      {
        await Connection.OpenAsync();
      }

      var command = Connection.CreateCommand();
      command.Connection = Connection;
      command.CommandText = commandText;
      await command.ExecuteNonQueryAsync();
    }

    public static async Task<HttpStatusCode> ExecuteSqlTransactionAsync(string commandText)
    {
      while (Reading)
      {
        await Task.Delay(TimeSpan.FromSeconds(readWaitSeconds));
      }

      SqlCeTransaction transaction = null;

      try
      {
        if (Connection.State == ConnectionState.Closed)
        {
          await Connection.OpenAsync();
        }

        transaction = await Task.Run(() => Connection.BeginTransaction());

        SqlCeCommand command = Connection.CreateCommand();
        command.Connection = Connection;
        command.Transaction = transaction;
        command.CommandText = commandText;
        await command.ExecuteNonQueryAsync();

        transaction.Commit();
        return HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
        if (transaction != null)
        {
          try
          {
            transaction.Rollback();
            if (ex.Message.Contains("Violation of UNIQUE KEY constraint"))
            {
              return HttpStatusCode.BadRequest;
            }
            else
            {
              return HttpStatusCode.InternalServerError;
            }
          }
          catch (Exception innerEx)
          {
            await Log(innerEx);
            return HttpStatusCode.InternalServerError;
          }
        }
        await Log(ex);
        if (ex.Message.Contains("Violation of UNIQUE KEY constraint"))
        {
          return HttpStatusCode.BadRequest;
        }
        else
        {
          return HttpStatusCode.InternalServerError;
        }
      }
    }

    public static void ExecuteSqlTransaction(string commandText)
    {
      while (Reading)
      {
        Task.Delay(TimeSpan.FromSeconds(readWaitSeconds));
      }

      SqlCeTransaction transaction = null;

      try
      {
        if (Connection.State == ConnectionState.Closed)
        {
          Connection.Open();
        }

        transaction = Connection.BeginTransaction();

        var command = Connection.CreateCommand();
        command.Connection = Connection;
        command.Transaction = transaction;
        command.CommandText = commandText;
        command.ExecuteNonQuery();

        transaction.Commit();
      }
      catch (Exception ex)
      {
        if (transaction != null)
        {
          try
          {
            transaction.Rollback();
          }
          catch (Exception innerEx)
          {
            Task.Run(() => Log(innerEx));
          }
        }
        Task.Run(() => Log(ex));
      }
    }
  }
}
