import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { of, from } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuardService implements CanActivate {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private angularFireAuth: AngularFireAuth
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    return this.angularFireAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return from(this.authenticationService.getCurrentUserClaim(user)).pipe(
            map((claims: any) => {
              if (!claims.admin) {
                console.log('access denied');
                this.router.navigate(['/login']);
              }
              return !!claims.admin;
            })
          );
        } else {
          console.log('access denied');
          this.router.navigate(['/login'])          
          return of(false)
        }
      })
    );
  }
}
