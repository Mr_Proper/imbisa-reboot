import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ArticleBaseService } from './article-base.service';
import { IArticleService } from './article-service.interface';

@Injectable({
  providedIn: 'root'
})
export class ArticleService extends ArticleBaseService implements IArticleService {

  constructor(
    private angularFirestore: AngularFirestore
  ) {
    super(angularFirestore, 'articles');
  }
}
