import { Component, AfterViewInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthenticationService } from '../authentication.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-list-administrators',
  templateUrl: './list-administrators.component.html'
})
export class ListAdministratorsComponent implements AfterViewInit {

  users = [];
  options = [];

  constructor(
    private angularFirestore: AngularFirestore,
    private authenticationService: AuthenticationService,
    private angularFireAuth: AngularFireAuth 
  ) {
    this.getUsers();
  }

  ngAfterViewInit() {
    this.options = [
      { key: false, value: $('#listAdminOptionNo').text() },
      { key: true, value: $('#listAdminOptionYes').text() }
    ];
  }

  getUsers() {
    this.users = [];
    this.angularFirestore
      .collection('users').ref
      .orderBy('email')
      .limit(10)           
      .get()
      .then((querySnapshot: any) => {
        querySnapshot.forEach(doc => {
          let copy = Object.assign({ id: doc.id }, doc.data());
          copy.admin = !!copy.admin;
          copy.notifyNewArticle = !!copy.notifyNewArticle;
          this.users.push(copy);
        });
      });
  }

  changeAdmin(user) {
    user.disableAdmin = true;
    this.authenticationService.postClaim(user)
    .then(() => user.disableAdmin = false)
    .catch(error => {
      user.admin = user.admin == 'true' ? 'false' : 'false';
      user.disableAdmin = false;
      alert($('#listAdminChangeAdminFail').text());
    });
  }

  changeNotification(user){
    user.disableNotification = true;
    this.angularFirestore
      .collection('users')
      .doc(user.id).set({
        notifyNewArticle: user.notifyNewArticle == 'true' ? true : false
      }, {merge: true})
      .then(() => user.disableNotification = false)
      .catch(error => {
        user.notifyNewArticle = user.notifyNewArticle == 'true' ? 'false' : 'true';
        user.disableNotification = false;
        alert($('#listAdminChangeAdminFail').text());
      })
  }
}
