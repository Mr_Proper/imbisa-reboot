import { Component, OnInit, ElementRef, ViewChild, Input, OnDestroy } from '@angular/core';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

declare var ImageCompressor: any;
const compressor = new ImageCompressor();

@Component({
  selector: 'app-image-uploader',
  templateUrl: './image-uploader.component.html'
})
export class ImageUploaderComponent implements OnInit, OnDestroy {

  @ViewChild('fileInput', { read: ElementRef, static: true }) fileInput;
  @Input() model;
  @Input() form;

  private maxImageSizeKB = 2000;
  currentImageName;

  constructor() { }

  ngOnInit() {
    if (this.model.imageName && this.model.imageName.length) {
      this.currentImageName = this.model.imageName;
    }

    this.form.addControl(this.model.id, new FormControl(''));
    setTimeout(self => {
      self.form.get(self.model.id).setValidators([self.imageValidator()]);
      self.form.get(self.model.id).updateValueAndValidity();
    }, 10, this);

    this.model.uploader = new FileUploader({ url: 'URL', removeAfterUpload: true });
    this.model.uploader._xhrTransport = this.handleRequest.bind(this);
    this.model.uploader.onBeforeUploadItem = (fileItem) => this.onBeforeUploadItem(fileItem);
    this.model.uploader.updateModel = () => this.updateModel();
  }

  imageValidator(nameRe: RegExp): ValidatorFn {
    return () => {
      let invalidMessage;
      if (!$(this.fileInput.nativeElement).val() && !this.currentImageName) {
        invalidMessage = 'Image required';
      }
      if (!invalidMessage && Array.isArray(this.model.uploader.queue) && this.model.uploader.queue.length > 0) {
        let currentFileItem = this.model.uploader.queue[this.model.uploader.queue.length - 1];
        invalidMessage = this.validateImage(currentFileItem);
      }
      return invalidMessage ? { 'imageName': { value: invalidMessage } } : null;
    };
  }

  private updateModel() {
    let self = this;
    return new Promise((resolve) => {
      self.model.uploader.onSuccessItem = (fileItem, content) => {

        let fileItemSizeKB = this.convertToKilobytes(fileItem.file.size);
        self.model.imageName = fileItem.file.name;
        self.model.content = content;
        self.model.large = fileItemSizeKB > 500;
        resolve();
      };
      if (Array.isArray(self.model.uploader.queue) && self.model.uploader.queue.length > 0) {
        let lastFileItem = self.model.uploader.queue[self.model.uploader.queue.length - 1];
        self.model.uploader.queue = [lastFileItem];
        self.model.uploader.uploadAll();
      } else {
        resolve();
      }
    });
  }

  ngOnDestroy() {
    delete this.model.uploader;
  }

  clearImageUpload() {
    this.model.imageName = '';
    this.currentImageName = '';
    this.model.content = 'data:image';
    this.model.uploader.clearQueue();
    $(this.fileInput.nativeElement).val('');
    this.form.get(this.model.id).updateValueAndValidity();
  }

  private onBeforeUploadItem(fileItem) {
    if (fileItem.file.type == 'image/jpeg') {
      fileItem.file.name = fileItem.file.name.replace('.jpeg', '.jpg');
      fileItem.file.type = 'image/jpg';
    }
    return fileItem;
  }

  private validateImage(fileItem) {
    let fileItemSizeKB = this.convertToKilobytes(fileItem.file.size);
    if (fileItemSizeKB > this.maxImageSizeKB) {
      return 'Image size cannot exceed ' + this.maxImageSizeKB + 'KB.';
    }
  }

  private convertToKilobytes(bytes) {
    return bytes / 1025;
  }

  private handleRequest(fileItem) {
    this.model.uploader._onBeforeUploadItem(fileItem);
    let self = this;

    this.compressImage(fileItem).then(rawFile => {

      let fileReader = new FileReader();
      fileReader.readAsDataURL(rawFile);
      fileReader.onload = () => {
        self.model.uploader._onSuccessItem(fileItem, fileReader.result);
        self.model.uploader._onCompleteItem(fileItem);
      };
      this.model.uploader._render();
    });
  }

  private compressImage(fileItem) {
    let fileItemSizeKB = this.convertToKilobytes(fileItem.file.size);

    let quality;
    if (fileItemSizeKB > 1500) {
      quality = .4;
    } else if (fileItemSizeKB > 1000) {
      quality = .6;
    } else {
      quality = .8;
    }

    return compressor.compress(fileItem.file.rawFile, {
      quality: quality,
      convertSize: Infinity
    });
  }

  imageSelected() {
    this.currentImageName = '';
    this.form.get(this.model.id).updateValueAndValidity();
  }

  download(){
    let link = document.createElement('a');
    link.href = this.model.content;
    link.download = this.model.imageName;
    link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
    setTimeout(() => {
        window.URL.revokeObjectURL(link.href);
        link.remove();
    }, 100);
  }  
}
