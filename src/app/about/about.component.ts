import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html'
})
export class AboutComponent implements OnDestroy {

  private routeSubscription;
  private navigationSubscription;
  private currentParams;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    //private scrollToService: ScrollToService,
  ) {
    setTimeout(() => this.scrollToSection(), 1000, this);
  }

  private scrollToSection() {
    this.routeSubscription = this.route.params
      .subscribe(params => {
        this.currentParams = params;
        //this.scrollToService.scrollTo({ target: this.currentParams.section, offset: -184 });
      }, error => console.log(error));
    this.navigationSubscription = this.router.events.subscribe(e => {
      //if (e instanceof NavigationEnd && this.currentParams)
        //this.scrollToService.scrollTo({ target: this.currentParams.section, offset: -184 });
    }, error => console.log(error));
  }

  ngOnDestroy() {
    if (this.routeSubscription)
      this.routeSubscription.unsubscribe();
    if (this.navigationSubscription)
      this.navigationSubscription.unsubscribe();
  }
}
