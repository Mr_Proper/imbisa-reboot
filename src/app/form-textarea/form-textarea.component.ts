import { Component, OnInit, Input, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-textarea',
  templateUrl: './form-textarea.component.html'
})
export class FormTextareaComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input() section;
  @Input() form;
  @ViewChild('textarea') textarea;

  constructor() { }

  ngAfterViewInit() {
    this.autoExpand(this.textarea.nativeElement);
  }

  ngOnInit() {
    if(this.form){
      this.form.addControl(this.section.id, new FormControl(this.section.content));
      setTimeout(self => {
        self.form.get(self.section.id).setValidators([Validators.required]);
        self.form.get(self.section.id).updateValueAndValidity();     
      },0, this);
    }
  }

  ngOnDestroy(){
    this.section.bold = false;
    this.section.italic = false;
    this.section.fontSize = '0.9rem';
  }

  autoExpand(field) {

    // Reset field height
    field.style.height = 'inherit';

    // Get the computed styles for the element
    let computed = window.getComputedStyle(field);

    // Calculate the height
    let height = parseInt(computed.getPropertyValue('border-top-width'), 10)
      + parseInt(computed.getPropertyValue('padding-top'), 10)
      + field.scrollHeight
      + parseInt(computed.getPropertyValue('padding-bottom'), 10)
      + parseInt(computed.getPropertyValue('border-bottom-width'), 10);

    field.style.height = (Math.round((height / 16) * 100) / 100) + 'rem';
  };
}
