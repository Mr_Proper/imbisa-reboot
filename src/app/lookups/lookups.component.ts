import { Component } from '@angular/core';
import { RegionService } from '../region.service';
import { TopicService } from '../topic.service';
import { AuthorService } from '../author.service';

@Component({
  selector: 'app-lookups',
  templateUrl: './lookups.component.html'
})
export class LookupsComponent {

  constructor(
    public authorService: AuthorService,
    public regionService: RegionService,
    public topicService: TopicService
  ) { }
}
