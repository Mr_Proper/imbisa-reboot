import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MonthsService {

  months = [];
  currentMonthName = '';
  last6MonthsName = $('#monthLast6').text();

  constructor() {
    this.months = this.getMonthList(24);
  }

  private getMonthNames(){
    return [
      $('#monthJan').text(),
      $('#monthFeb').text(),
      $('#monthMar').text(),
      $('#monthApr').text(),
      $('#monthMay').text(),
      $('#monthJun').text(),
      $('#monthJul').text(),
      $('#monthAug').text(),
      $('#monthSep').text(),
      $('#monthOct').text(),
      $('#monthNov').text(),
      $('#monthDec').text(),
    ];
  }

  private getMonthList(monthCount){
    let monthNames = this.getMonthNames();
    let currentYear = new Date().getFullYear();
    let currentMonth = new Date().getMonth();
    let months = [];
    months.push({
      name: this.last6MonthsName,
      startDate: new Date(currentYear, currentMonth - 5).toISOString(),
      endDate: new Date(currentYear, currentMonth + 1).toISOString()
    });

    for(let i = 0; i < monthCount; i++){
      months.push({
        name: monthNames[currentMonth] + ' ' + currentYear,
        startDate: new Date(currentYear, currentMonth).toISOString(),
        endDate: new Date(currentYear, currentMonth + 1).toISOString()
      });
      currentMonth--;
      if(currentMonth < 0){
        currentMonth = 11;
        currentYear--;
      }
    }
    return months;
  }  
}
