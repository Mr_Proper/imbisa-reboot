import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-document-link',
  templateUrl: './document-link.component.html'
})
export class DocumentLinkComponent implements OnInit {

  @Input() form;
  descriptionId = 'documentLinkDescription';
  urlId = 'documentLinkUrl';

  constructor() { }

  ngOnInit() {
    if(this.form){
      this.form.addControl(this.descriptionId, new FormControl(''));
      this.form.addControl(this.urlId, new FormControl(''));
    }
  }
}
