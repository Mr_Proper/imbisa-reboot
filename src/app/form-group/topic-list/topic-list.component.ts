import { Component, OnInit, Input } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { TopicService } from 'src/app/topic.service';

@Component({
  selector: 'app-topic-list',
  templateUrl: './topic-list.component.html'
})
export class TopicListComponent implements OnInit {

  @Input() form;
  id = 'topic';

  private topicsSubscription;

  constructor(
    public topicService: TopicService
  ) { 
    this.topicsSubscription = this.topicService
    .get()
    .subscribe(() => { }, error => console.log(error));
  }

  ngOnInit() {
    if(this.form){
      this.form.addControl(this.id, new FormControl('', Validators.required));
    }
  }

  ngOnDestroy() {
    if (this.topicsSubscription)
      this.topicsSubscription.unsubscribe();
  }  
}
