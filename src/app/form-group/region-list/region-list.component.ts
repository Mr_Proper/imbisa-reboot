import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { RegionService } from 'src/app/region.service';

@Component({
  selector: 'app-region-list',
  templateUrl: './region-list.component.html'
})
export class RegionListComponent implements OnInit, OnDestroy {

  @Input() form;
  id = 'region';

  private regionsSubscription;

  constructor(
    public regionService: RegionService
  ) {
    this.regionsSubscription = this.regionService
      .get()
      .subscribe(() => { }, error => console.log(error));
  }

  ngOnInit() {
    if(this.form){
      this.form.addControl(this.id, new FormControl(''));
      setTimeout(self => {
        self.form.get(self.id).setValidators([Validators.required]);
        self.form.get(self.id).updateValueAndValidity();     
      },0, this);
    }
  }

  ngOnDestroy() {
    if (this.regionsSubscription)
      this.regionsSubscription.unsubscribe();
  }
}
