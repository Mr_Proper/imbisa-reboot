import { Component, OnInit, Input } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-publish-date',
  templateUrl: './publish-date.component.html'
})
export class PublishDateComponent implements OnInit {

  @Input() form;
  id = 'publishDate';

  constructor() { }

  ngOnInit() {
    if(this.form){
      this.form.addControl(this.id, new FormControl(''));
      setTimeout(self => {
        self.form.get(self.id).setValidators([Validators.required]);
        self.form.get(self.id).updateValueAndValidity();     
      },0, this);
    }
  }
}
