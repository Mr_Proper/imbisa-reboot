import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-visibility-list',
  templateUrl: './visibility-list.component.html'
})
export class VisibilityListComponent implements OnInit {

  @Input() form;
  @Output() change = new EventEmitter();
  id = 'visibility';

  constructor() { }

  ngOnInit() {
    if(this.form){
      this.form.addControl(this.id, new FormControl('Hidden'));
      setTimeout(self => {
        self.form.get(self.id).setValidators([Validators.required]);
        self.form.get(self.id).updateValueAndValidity();     
      },0, this);
    }
  }

  changeVisibility(){
    this.change.emit();   
  }
}
