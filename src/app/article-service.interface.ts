export interface IArticleService{
    article;
    sections;
    prevArticleId;
    newArticle();
    newSections();
    getArticle(id, locale);
    getSections(id, locale);
    delete(id, locale);
}