import { QueryService } from './query.service';
import { TopicService } from './topic.service';
import { MonthsService } from './months.service';
import { AuthorService } from './author.service';
import { RegionService } from './region.service';

export class QueryBaseComponent {

  query;
  private topicsSubscription;
  private regionsSubscription;
  private authorsSubscription;

  constructor(
    private _queryService: QueryService,
    private _topicService: TopicService,
    private _monthsService: MonthsService,
    private _authorService: AuthorService,
    private _regionService: RegionService,
  ) {
    this._monthsService.currentMonthName = '';
    this.query = _queryService.newQuery();
    this.topicsSubscription = this._topicService
      .get()
      .subscribe(() => { }, error => console.log(error));
    this.regionsSubscription = this._regionService
      .get()
      .subscribe(() => { }, error => console.log(error));
    this.authorsSubscription = this._authorService
      .get()
      .subscribe(() => { }, error => console.log(error));
  }

  onDestroy() {
    if (this.topicsSubscription)
      this.topicsSubscription.unsubscribe();
    if (this.regionsSubscription)
      this.regionsSubscription.unsubscribe();
    if (this.authorsSubscription)
      this.authorsSubscription.unsubscribe();
  }

  clearSearch() {
    this.query.topic = '';
    this.query.author = '';
    this.query.region = '';
    this.query.startDate = null;
    this.query.endDate = null;
    this._monthsService.currentMonthName = '';
    this.search()
      .then(() => { })
      .catch(error => console.log(error));
  }

  changeDate() {
    let currentMonth = this._monthsService.months.find(month => month.name == this._monthsService.currentMonthName);
    this.query.startDate = currentMonth ? currentMonth.startDate : null;
    this.query.endDate = currentMonth ? currentMonth.endDate : null;
    return this.search();
  }

  moreResults() {
    this.query.currentIndex += this.query.limit;
    this._queryService
      .search(this.query)
      .then(() => { })
      .catch(error => console.log(error));
  }

  search() {
    this.query.currentIndex = 0;
    return this._queryService
      .search(this.query);
  }
}