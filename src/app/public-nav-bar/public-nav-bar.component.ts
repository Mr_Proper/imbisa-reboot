import { Component, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-public-nav-bar',
  templateUrl: './public-nav-bar.component.html'
})
export class PublicNavBarComponent implements AfterViewInit {

  currentPath = '';
  locale;
  displayFlags;
  loginPage;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) { 
    this.locale = window.location.href.indexOf('/pt/') > -1 ? 'pt' : 'en';    
    this.currentPath = this.route.snapshot.url[0].path;
    this.loginPage = this.currentPath.indexOf('login') > -1;
  }

  ngAfterViewInit() {

    $('a[routerLink]').each((index, elem) => {
      if ($(elem).attr('routerLink').indexOf(this.currentPath) > -1) {
        $(elem).addClass('active');
      }
    });
    $('a[id]').each((index, elem) => {
      if ($(elem).attr('id').indexOf(this.currentPath) > -1) {
        $(elem).addClass('active');
      }
    });
    $('button[id]').each((index, elem) => {
      if ($(elem).attr('id').indexOf(this.currentPath) > -1) {
        $(elem).addClass('selected');
      }
    });    
  }

  manage() {
    if (this.authenticationService.admin) {
      this.router.navigate(['/articles']);
    }
  }

  stories(){
    this.router.navigate(['/stories']);   
  }
}
