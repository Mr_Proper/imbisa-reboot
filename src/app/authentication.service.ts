import { Injectable, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService implements OnDestroy {

  private baseUrl = 'https://us-central1-imbisaapp.cloudfunctions.net/widgets/claims';
  private userSubscription;
  admin;

  constructor(
    private angularFireAuth: AngularFireAuth,
    private router: Router,
    private http: HttpClient
  ) {
    this.userSubscription = this.angularFireAuth.authState
      .subscribe(user => {
        this.getCurrentUserClaim(user)
          .then((claim: any) => this.admin = !!claim.admin)
          .catch(error => console.log(error))
      });
  }

  ngOnDestroy() {
    if (this.userSubscription)
      this.userSubscription.unsubscribe();
  }

  googleLogin() {
    let provider = new auth.GoogleAuthProvider();
    this.angularFireAuth.signInWithPopup(provider)
      .then(() => {})
      .catch(error => console.log(error));
  }

  signOut() {
    this.angularFireAuth.signOut()
      .then(() => {
        this.admin = false;
        this.router.navigate(['/login']);
      })
      .catch(error => console.log(error));
  }

  getCurrentUserClaim(user) {
    return new Promise((resolve, reject) => {
      if (user) {
        user.getIdTokenResult(true)
          .then((token: any) => resolve(token.claims))
          .catch(error => reject(error));
      } else {
        resolve({});
      }
    });
  }

  postClaim(user) {
    return new Promise((resolve, reject) => {
      this.angularFireAuth.currentUser
        .then(currentUser => {
          currentUser.getIdTokenResult(true)
          .then(tokenResult => {
            user.admin = user.admin == "true";
            user.token = tokenResult.token;
            this.http.post(this.baseUrl, user).subscribe(() => resolve(), error => reject(error));
          })
          .catch(error => reject(error));          
        })
        .catch(error => reject(error));
    });
  }
}
