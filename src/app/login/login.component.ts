import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService,
    private angularFireAuth: AngularFireAuth
  ) { }

  ngOnInit() {
  }

  createAdmin(){
    this.angularFireAuth.currentUser.then(user => {
      this.authenticationService.postClaim({
        id: user.uid,
        admin: true,
        visible: true
      });
    });
  }
}
