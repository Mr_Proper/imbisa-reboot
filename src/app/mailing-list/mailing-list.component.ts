import { Component, OnDestroy, ViewContainerRef } from '@angular/core';
import { SubscriberService } from '../subscriber.service';
import { ModalDialogService } from 'ngx-modal-dialog';
import { MailingListModalComponent } from '../mailing-list-modal/mailing-list-modal.component';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-mailing-list',
  templateUrl: './mailing-list.component.html'
})
export class MailingListComponent implements OnDestroy {

  private subscriberSubscription;
  disableSubmit;
  email;

  constructor(
    private subscriberService: SubscriberService,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef
  ) {
    this.subscriberSubscription = this.subscriberService
      .get()
      .subscribe(() => { }, error => console.log(error));
  }

  ngOnDestroy() {
    if (this.subscriberSubscription)
      this.subscriberSubscription.unsubscribe();
  }

  saveSubscriber(valid) {
    if (valid) {
      let subscriber = this.subscriberService.list.find(x => x.email == this.email);
      if (!subscriber) {
        this.disableSubmit = true;
        this.subscriberService.save(this.email)
          .then(() => this.openModal())
          .catch(error => {
            console.log(error);
            this.disableSubmit = false;
          });
      } else {
        alert($('#subcriberExists').text());
      }
    } else {
      alert($('#invalidEmailAddress').text());
    }
  }

  openModal() {
    this.modalService.openDialog(this.viewRef, {
      childComponent: MailingListModalComponent,
      data: {
        email: this.email
      },
      settings: {
        overlayClass: '',
        headerClass: '',
      },
      closeDialogSubject: new Subject(),
      onClose: () => {       
        this.email = null;
        this.disableSubmit = false;       
        return true;
      }             
    });
  }  
}
