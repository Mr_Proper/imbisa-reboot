import { Component, ComponentRef} from '@angular/core';
import { IModalDialog, IModalDialogOptions } from 'ngx-modal-dialog';

@Component({
  selector: 'app-stories-modal',
  templateUrl: './stories-modal.component.html'
})
export class StoriesModalComponent implements IModalDialog {
  
  options;

  constructor() { }

  dialogInit(
    reference: ComponentRef<IModalDialog>,
    options: Partial<IModalDialogOptions<any>>
  ) {
    this.options = options;
  }

  proceed(){
    this.options.closeDialogSubject.next();
    this.options.data.resolve(true);   
  }

  closeModal(){
    this.options.closeDialogSubject.next();
    this.options.onClose();
  }
}
