import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ListService } from './list.service';

@Injectable({
  providedIn: 'root'
})
export class RegionService extends ListService {

  constructor(
    private angularFirestore: AngularFirestore
  ) {
    super(angularFirestore, 'regions');
  }
}
