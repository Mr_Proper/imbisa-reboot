import { Component } from '@angular/core';
import { SubscriberService } from '../subscriber.service';

@Component({
  selector: 'app-list-subscribers',
  templateUrl: './list-subscribers.component.html'
})
export class ListSubscribersComponent {

  constructor(
    public subscriberService: SubscriberService
  ) { }
}
