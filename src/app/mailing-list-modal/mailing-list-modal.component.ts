import { Component, ComponentRef } from '@angular/core';
import { IModalDialog, IModalDialogOptions } from 'ngx-modal-dialog';
import { SubscriberService } from '../subscriber.service';

@Component({
  selector: 'app-mailing-list-modal',
  templateUrl: './mailing-list-modal.component.html'
})
export class MailingListModalComponent implements IModalDialog {

  options;

  constructor(
    private subscriberService: SubscriberService
  ) { }

  dialogInit(
    reference: ComponentRef<IModalDialog>,
    options: Partial<IModalDialogOptions<any>>
  ) {
    this.options = options;
  }

  closeModal(){
    this.options.closeDialogSubject.next();
    this.options.onClose();
  }

  deleteSubscriber(){
    this.subscriberService.deleteEmail(this.options.data.email)
    .then(() => this.closeModal())
    .catch(error => console.log(error));
  }
}
