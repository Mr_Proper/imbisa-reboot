import { Component, OnDestroy, OnInit } from '@angular/core';
import { QueryService } from '../query.service';
import { MonthsService } from '../months.service';
import { Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { TopicService } from '../topic.service';
import { AuthorService } from '../author.service';
import { RegionService } from '../region.service';
import { QueryBaseComponent } from '../query-base.component';
import { PublicArticleService } from '../public-article.service';

@Component({
  selector: 'app-list-articles',
  templateUrl: './list-articles.component.html'
})
export class ListArticlesComponent extends QueryBaseComponent implements OnDestroy, OnInit  {

  private aboveBottomOfContent;
  private locale;

  constructor(
    public queryService: QueryService,
    public topicService: TopicService,
    public monthsService: MonthsService,
    private router: Router,
    private articleService: ArticleService,
    private publicArticleService: PublicArticleService,
    public authorService: AuthorService,
    public regionService: RegionService
  ) {
    super(queryService,
      topicService,
      monthsService,
      authorService,
      regionService);
    this.locale = window.location.href.indexOf('/pt/') > -1 ? 'pt' : 'en';
    
    queryService.enableInitialSearchWhenNoParameters = false;
    this.query.limit = 5;
    this.query.visibility = 'PublicHidden';
    this.listArticlesSearch();
  }

  listArticlesSearch() {
    this.search()
      .then(() => this.triggerMoreResults())
      .catch(error => console.log(error));
  }

  listArticlesChangeDate() {
    this.changeDate()
      .then(() => this.triggerMoreResults())
      .catch(error => console.log(error));
  }

  ngOnInit() {
    $(window).scroll(() => {
      this.triggerMoreResults();
    });  
  }

  private triggerMoreResults() {
    let bottomOfContent = $(window).scrollTop() + $(window).height() > $(document).height() - 640;
    if (bottomOfContent) {
      if (this.aboveBottomOfContent) {
        this.aboveBottomOfContent = false;
        if (!(this.queryService.queryEnd || this.queryService.searching)) {
          this.moreResults();
        }
      }
    } else {
      this.aboveBottomOfContent = true;
    }
  }

  ngOnDestroy() {
    this.onDestroy();
  }

  deleteArticle(article) {
    let result = confirm($('#listArticleConfirmDelete').text() + ' (' + article.articleHeadline + ')');
    let articleService = article.visibility ? this.publicArticleService : this.articleService;
    if (!result) {
      return;
    }

    article.disabled = true;
    articleService.delete(article.id, this.locale)
      .then(() => {
        this.queryService.removeDocument(article.id)
        article.disabled = false;
      })
      .catch(error => {
        article.disabled = false;
        alert($('#listArticleDeleteFail').text());
      });
  }

  editArticle(article) {
    this.router.navigate(['/articles/' + article.id + '/edit' + (article.visibility ? '/public' : '')]);
  }

  backToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }
}
