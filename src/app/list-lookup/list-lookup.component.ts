import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ListService } from '../list.service';

@Component({
  selector: 'app-list-lookup',
  templateUrl: './list-lookup.component.html'
})
export class ListLookupComponent implements OnDestroy, OnInit {

  @Input('service') lookupService: ListService;
  @Input() disableAdd;
  @Input() firstColumnName;

  private subscription;
  name;

  ngOnInit() {
    this.firstColumnName = this.firstColumnName || 'Name';
    this.subscription = this.lookupService
      .get()
      .subscribe(() => { }, error => console.log(error));
  }

  ngOnDestroy() {
    if (this.subscription)
      this.subscription.unsubscribe();
  }

  save() {
    let exists = this.lookupService.list.find(x => x.name == this.name);
    if(exists){
      alert($('#listLookupItemAlreadyExists').text());
    }else{
      this.lookupService.save(this.name)
      .then(() => {
        alert($('#listLookupSuccessfullySubmitted').text());
        this.name = null;
      })
      .catch(error => console.log(error));
    }
  }

  delete(item) {
    let result = confirm($('#listLookupConfirmDelete').text());
    if (!result) {
      return;
    }

    item.disabled = true;
    this.lookupService.delete(item)
      .then(() => item.disabled = false)
      .catch(error => {
        item.disabled = false;
        alert($('#listLookupDeleteFail').text());
      });  
  }
}
