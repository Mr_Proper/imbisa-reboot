import { Component, OnDestroy } from '@angular/core';
import { LinkService } from '../link.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnDestroy {

  private linksSubscription;

  constructor(
    public linkService: LinkService,
    public domSanitizer: DomSanitizer
  ) {
    this.linksSubscription = this.linkService
      .get()
      .subscribe(() => { }, error => console.log(error));
  }

  ngOnDestroy() {
    if (this.linksSubscription)
      this.linksSubscription.unsubscribe();
  }
}
