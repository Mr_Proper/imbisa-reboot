import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class LoginRequestService {

  list;
  private collectionName = 'login-requests';

  constructor(
    private angularFirestore: AngularFirestore,
    private angularFireAuth: AngularFireAuth
  ) { }

  get() {
    this.list = [];
    return this.angularFirestore
      .collection(this.collectionName)
      .get()
      .pipe(map((querySnapshot: any) => {
        querySnapshot.forEach(doc => {
          let item = Object.assign({ id: doc.id }, doc.data());
          this.list.push(item);
        });
        return this.list;
      }));
  }

  save(email) {
    return new Promise((resolve, reject) => {
      this.angularFireAuth.currentUser.then(user => {
        this.angularFirestore
        .collection(this.collectionName)
        .add({ 
          existingUserEmail: user.email,
          newUserEmail: email 
        })
        .then(item => {
          this.list.push(item);
          resolve(item.id);
        }).catch(error => reject(error));
      });
    });
  }
}
