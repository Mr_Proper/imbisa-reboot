import { Component, OnDestroy, OnInit } from '@angular/core';
import { QueryService } from '../query.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MonthsService } from '../months.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { TopicService } from '../topic.service';
import { AuthorService } from '../author.service';
import { RegionService } from '../region.service';
import { QueryBaseComponent } from '../query-base.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent extends QueryBaseComponent implements OnDestroy, OnInit {

  private aboveBottomOfContent;
  private routeSubscription;
  private navigationSubscription;
  private currentParams;

  get articles() {
    let articles = [];
    let currentRow = 0;
    let currentCol = 0;

    let tomorrow = new Date();
    tomorrow.setDate(new Date().getDate()+1);
    let nextDay = new Date(tomorrow.getFullYear(), tomorrow.getMonth(), tomorrow.getDate()).toISOString();
    let filteredResults = this.queryService.queryDocuments.filter(x => x[this.queryService.dateColumnName] < nextDay);

    filteredResults.forEach(article => {
      if (!articles[currentRow]) {
        articles[currentRow] = [];
      }
      articles[currentRow][currentCol] = article;
      if (currentCol == 2) {
        currentRow++;
        currentCol = 0;
      } else {
        currentCol++;
      }
    });

    return articles;
  }

  constructor(
    public queryService: QueryService,
    public topicService: TopicService,
    private domSanitizer: DomSanitizer,
    public monthsService: MonthsService,
    public authorService: AuthorService,
    public regionService: RegionService,
    private router: Router,
    private route: ActivatedRoute,
    //private scrollToService: ScrollToService
  ) {
    super(queryService,
      topicService,
      monthsService,
      authorService,
      regionService);

    queryService.enableInitialSearchWhenNoParameters = true; 
    queryService.locales = ['en', 'pt']; 
    this.query.limit = 6;
    this.query.visibility = 'Public';
    this.homeSearch();
    setTimeout(() => this.scrollToSection(), 1000, this);
  }

  homeSearch() {
    this.search()
      .then(() => this.triggerMoreResults())
      .catch(error => console.log(error));
  }

  homeChangeDate() {
    this.changeDate()
      .then(() => this.triggerMoreResults())
      .catch(error => console.log(error));
  }

  private scrollToSection() {
    this.routeSubscription = this.route.params
      .subscribe(params => {
        this.currentParams = params;
        //this.scrollToService.scrollTo({ target: this.currentParams.section, offset: -128 });
      }, error => console.log(error));
    this.navigationSubscription = this.router.events.subscribe(e => {
      //if (e instanceof NavigationEnd && this.currentParams)
        //this.scrollToService.scrollTo({ target: this.currentParams.section, offset: -128 });
    }, error => console.log(error));
  }

  ngOnInit() {
    $(window).scroll(() => {
      $('.home-top-image-content > .text-center').css('margin-top', "-" + $(document).scrollTop() + "px");
      this.triggerMoreResults();
    });
  }

  private triggerMoreResults() {
    let bottomOfContent = $(window).scrollTop() + $(window).height() > $(document).height() - 603.6;
    let mobile = $(window).width() < 576;
    if (mobile) {
      bottomOfContent = $(window).scrollTop() + $(window).height() > $(document).height() - 228.4;
    }
    if (bottomOfContent) {
      if (this.aboveBottomOfContent) {
        this.aboveBottomOfContent = false;
        if (!(this.queryService.queryEnd || this.queryService.searching)) {
          this.moreResults();
        }
      }
    } else {
      this.aboveBottomOfContent = true;
    }
  }

  ngOnDestroy() {
    this.onDestroy();
  }

  viewArticle(article) {
    this.router.navigate(['/articles/' + article.id + '/view'], { queryParams: { locale: article.locale } });
  }

  viewStories() {
    this.router.navigate(['/stories']);
  }

  backToTop() {
    this.router.navigate(['/home/content-top']);
  }
}
