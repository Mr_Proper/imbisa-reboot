import { Component, Input, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { PublicArticleService } from '../public-article.service';

@Component({
  selector: 'app-view-article',
  templateUrl: './view-article.component.html'
})
export class ViewArticleComponent implements OnDestroy {

  @Input() article;
  @Input() sections = [];

  private queryParamsSubscription;
  private paramsSubscription;
  private topicsSubscription;
  private articleSubscription;
  private sectionsSubcription;
  displayLayout;
  window;

  constructor(
    public domSanitizer: DomSanitizer,
    private route: ActivatedRoute,
    public publicArticleService: PublicArticleService
  ) {
    this.window = window;
    if (window.location.href.indexOf('/view') > -1) {
      this.displayLayout = true;      
      this.paramsSubscription = this.route.params
        .subscribe(params => {
          this.queryParamsSubscription = this.route.queryParams.subscribe(queryParams => {
            let locale = window.location.href.indexOf('/pt/') > -1 ? 'pt' : 'en';
            if (queryParams.locale) {
              locale = queryParams.locale;
            }
            this.getArticle(params.id, locale);
          }, error => console.log(error));
        }, error => console.log(error));
    }
  }

  get twitterLink() {
    return 'https://twitter.com/share?ref_src=twsrc%5Etfw&text=Article Headline: ' + this.article.articleHeadline + ', Author: ' + this.article.authorName;
  }

  get facebookLink() {
    return 'https://www.facebook.com/dialog/share?app_id=533160507263023&display=popup&href=' + window.location.href + '&quote=Article Headline: ' + this.article.articleHeadline + ', Author: ' + this.article.authorName;
  }

  get whatsappLink() {
    return 'https://wa.me/?text=Article Headline: ' + this.article.articleHeadline + ', Author: ' + this.article.authorName + ', Link: ' + window.location.href;
  }

  getArticle(id, locale) {
    this.articleSubscription = this.publicArticleService
      .getArticle(id, locale)
      .subscribe(article => {
        if (article && Object.keys(article).length) {
          this.article = article;
          this.getSections(id, locale);
        }
      }, error => console.log(error));
  }

  getSections(id, locale) {
    this.sectionsSubcription = this.publicArticleService
      .getSections(id, locale)
      .subscribe(sections => {
        this.sections = sections;
        this.sections.sort((a, b) => a.order - b.order);
      }, error => console.log(error));
  }

  ngOnDestroy() {
    if (this.paramsSubscription)
      this.paramsSubscription.unsubscribe();
    if (this.queryParamsSubscription)
      this.queryParamsSubscription.unsubscribe();
    if (this.articleSubscription)
      this.articleSubscription.unsubscribe();
    if (this.topicsSubscription)
      this.topicsSubscription.unsubscribe();
    if (this.sectionsSubcription)
      this.sectionsSubcription.unsubscribe();
  }
}
