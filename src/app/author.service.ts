import { Injectable } from '@angular/core';
import { ListService } from './list.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class AuthorService extends ListService {

  constructor(
    angularFirestore: AngularFirestore
  ) {
    super(angularFirestore, 'authors')
  }
}
