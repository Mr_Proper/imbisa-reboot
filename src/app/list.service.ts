import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

export class ListService {

  list;
  private locale;

  constructor(
    private _angularFirestore: AngularFirestore,
    private _collectionName: string
  ) {
    this.locale = window.location.href.indexOf('/pt/') > -1 ? 'pt' : 'en';
  }

  get() {
    this.list = [];
    return this._angularFirestore
      .collection(this._collectionName + '-' + this.locale)
      .get()
      .pipe(map((querySnapshot: any) => {
        querySnapshot.forEach(doc => this.list.push(Object.assign({ id: doc.id }, doc.data())));
        this.list.sort((a, b) => (a.name > b.name) ? 1 : -1);
        return this.list;
      }));
  }

  save(name) {
    return new Promise((resolve, reject) => {
      return this._angularFirestore
        .collection(this._collectionName + '-' + this.locale)
        .add({ name: name })
        .then(item => {
          this.list.push({ id: item.id, name: name });
          this.list.sort((a, b) => (a.name > b.name) ? 1 : -1);
          resolve(item.id);
        }).catch(error => reject(error));
    });
  }

  delete(item) {
    return new Promise((resolve, reject) => {
      this._angularFirestore
      .doc(this._collectionName + '-' + this.locale + '/' + item.id)
      .delete()
      .then(() => {
        this.list.splice(this.list.indexOf(item), 1);
        resolve();
      })
      .catch(error => reject(error));
    });
  }

  getName(id) {
    let name = this.list
      .filter(x => x.id == id)
      .map(x => x.name);
    return name[0] || id;
  }
}
