import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { EditArticleComponent } from './edit-article/edit-article.component';
import { ModalDialogService } from 'ngx-modal-dialog';
import { StoriesModalComponent } from './stories-modal/stories-modal.component';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfirmDeactivateService implements CanDeactivate<EditArticleComponent> {

  constructor(
    private modalService: ModalDialogService     
  ) { }

  canDeactivate(component: EditArticleComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (component.hasChanges) {       
        this.openModal(component, resolve);
      }else{
        resolve(true);
      }
    });
  }

  openModal(component, resolve) {
    this.modalService.openDialog(component.viewRef, {
      childComponent: StoriesModalComponent,
      data: {
        resolve: status => resolve(status)
      },
      settings: {
        overlayClass: '',
        headerClass: '',
      },
      closeDialogSubject: new Subject(),
      onClose: () => {       
        resolve(false);     
        return true;
      }             
    });
  }   
}
