import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SubscriberService {

  list;
  private collectionName = 'subscribers';
  private locale;

  constructor(
    private angularFirestore: AngularFirestore
  ) {
    this.locale = window.location.href.indexOf('/pt/') > -1 ? 'pt' : 'en';
  }

  get() {
    this.list = [];
    return this.angularFirestore
      .collection(this.collectionName + '-' + this.locale)
      .get()
      .pipe(map((querySnapshot: any) => {
        querySnapshot.forEach(doc => {
          let item = Object.assign({ id: doc.id }, doc.data());
          item.name = item.email;
          this.list.push(item);
        });
        this.list.sort((a, b) => (a.email > b.email) ? 1 : -1);
        return this.list;
      }));
  }

  save(email) {
    return new Promise((resolve, reject) => {
      return this.angularFirestore
        .collection(this.collectionName + '-' + this.locale)
        .add({ email: email })
        .then(item => {
          this.list.push({ id: item.id, email: email, name: email });
          this.list.sort((a, b) => (a.email > b.email) ? 1 : -1);
          resolve(item.id);
        }).catch(error => reject(error));
    });
  }

  public deleteEmail(email) {
    return new Promise((resolve, reject) => {
      let subscriber = this.list.find(subscriber => subscriber.email == email);
      return this.angularFirestore
        .collection(this.collectionName + '-' + this.locale)
        .doc(subscriber.id)
        .delete()
        .then(() => {
          this.list.splice(this.list.indexOf(subscriber), 1);
          resolve();
        })
        .catch(error => reject(error));
    });
  }

  delete(item) {
    return new Promise((resolve, reject) => {
      this.angularFirestore
        .doc(this.collectionName + '-' + this.locale + '/' + item.id)
        .delete()
        .then(() => {
          this.list.splice(this.list.indexOf(item), 1);
          resolve();
        })
        .catch(error => reject(error));
    });
  }

  getName(id) {
    let email = this.list
      .filter(x => x.id == id)
      .map(x => x.email);
    return email[0] || id;
  }
}
