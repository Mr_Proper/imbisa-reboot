import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-content-article',
  templateUrl: './content-article.component.html'
})
export class ContentArticleComponent implements OnInit {

  @Input() content;
  html;

  ngOnInit() {
    let $div: any = $('<div>');
    this.content
    .split('\n')
    .filter(content => content.length)
    .forEach(content => $('<p>' + content + '</p>').appendTo($div)); 
    this.html = $div.html();  
  }
}
