import { Component, OnDestroy } from '@angular/core';
import { LoginRequestService } from '../login-request.service';

@Component({
  selector: 'app-login-reguest',
  templateUrl: './login-reguest.component.html'
})
export class LoginReguestComponent implements OnDestroy {

  private loginRequestSubscription;  
  email;
  disableSubmit;

  constructor(
    private loginReguestService: LoginRequestService
    ) {
      this.loginRequestSubscription = this.loginReguestService
        .get()
        .subscribe(() => { }, error => console.log(error));
    }
  
    ngOnDestroy() {
      if (this.loginRequestSubscription)
        this.loginRequestSubscription.unsubscribe();
    }

  saveLoginRequest(valid){
    if (valid) {
      let loginRequest = this.loginReguestService.list.find(x => x.newUserEmail == this.email);
      if (!loginRequest) {
        this.disableSubmit = true;
        this.loginReguestService.save(this.email)
          .then(() => {
            alert($('#loginRequestSuccessfullySubmitted').text());
            this.email = null;
            this.disableSubmit = false;      
          })
          .catch(error => {
            console.log(error);
            this.disableSubmit = false;
          });
      } else {
        alert($('#loginRequestExists').text());
      }
    } else {
      alert($('#invalidEmailAddress').text());
    }
  }
}
