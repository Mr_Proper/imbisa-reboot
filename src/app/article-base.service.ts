import { AngularFirestore } from '@angular/fire/firestore';
import { map, flatMap } from "rxjs/operators";
import { from, of } from 'rxjs';

export class ArticleBaseService {

  article;
  prevArticleId;
  sections;

  constructor(
    private _angularFirestore: AngularFirestore,
    private _collectionName: string
  ) { }

  newArticle() {
    this.article = {
      id: this._angularFirestore.createId()
    };
  }

  newSections() {
    this.sections = [];
  }

  getArticle(id, locale) {
    this.newArticle();
    return this._angularFirestore
      .collection(this._collectionName + '-' + locale)
      .doc(id)
      .get()
      .pipe(
        flatMap((doc: any) => {
          let article = doc.exists ? Object.assign({ id: doc.id }, doc.data()) : {};
          return this.getImagePart(article, locale);
        }));
  }

  getImagePart(article, locale) {
    if (!article.articleImageLarge) {
      this.article = article;
      return of(this.article);
    }
    return this._angularFirestore
      .collection('image-parts-' + locale)
      .doc(article.id)
      .get()
      .pipe(map((doc: any) => {
        article.articleImageContent += doc.data().articleImageContent + doc.data().articleImageContent2;
        this.article = article;
        return this.article;
      }));
  }

  getSections(id, locale) {
    this.newSections();
    return from(this._angularFirestore
      .collection('sections-' + locale).ref
      .where('article', '==', id)
      .get())
      .pipe(map(querySnapshot => {
        querySnapshot.forEach(doc => this.sections.push(Object.assign({ id: doc.id }, doc.data())));
        return this.sections;
      }));
  }

  _save(batch, article, sections, locale) {
    this.saveImagePart(batch, article, locale);
    this.saveArticle(batch, article, locale);
    sections
      .forEach(section => {
        let deleted = !section.new && this.sections.map(section => section.id).indexOf(section.id) == -1;
        if (deleted) {
          this.deleteSection(batch, section, locale);
        } else {
          section.article = this.article.id;
          this.saveSection(batch, section, locale);
        }
      });
  }

  private splitEqually(text, strCount) {

    let result = [];
    let resultString = '';
    let splitLength = Math.floor(text.length / strCount);
    let subStr;

    for (let i = 0; i < (strCount - 1); i++) {
      subStr = text.substring(resultString.length, resultString.length + splitLength);
      resultString += subStr;
      result.push(subStr);
    }

    subStr = text.substring(resultString.length);
    result.push(subStr);

    return result.filter(x => x.length);
  }

  delete(id, locale) {
    let batch = this._angularFirestore.firestore.batch();
    this.deleteImagePart(batch, id, locale);
    this.deleteArticle(batch, id, locale);
    return this.getSections(id, locale).pipe(
      map(sections => {
        sections
          .forEach(section => {
            this.deleteSection(batch, section, locale);
          });
      }),
      flatMap(() => batch.commit())
    ).toPromise();
  }

  deleteArticle(batch, id, locale) {
    return new Promise((resolve, reject) => {
      let ref = this._angularFirestore
        .collection(this._collectionName + '-' + locale)
        .doc(id).ref;

      ref.get()
        .then(querySnapshot => {
          if (querySnapshot.exists) {
            batch.delete(ref);
          }
          resolve();
        })
        .catch(error => reject(error));
    });
  }

  private saveArticle(batch, article, locale) {
    let ref = this._angularFirestore
      .collection(this._collectionName + '-' + locale)
      .doc(this.article.id).ref;

    this.cleanModel(article);
    batch.set(ref, article);
  }

  private cleanModel(model) {
    Object.keys(model).forEach(key => {
      if (model[key] === undefined) {
        delete model[key];
      }
    });
  }

  private saveSection(batch, section, locale) {
    let ref = this._angularFirestore
      .collection('sections-' + locale)
      .doc(section.id).ref;

    delete section.id;
    delete section.new;
    delete section.uploader;
    this.cleanModel(section);
    batch.set(ref, section);
  }

  private deleteSection(batch, section, locale) {
    let ref = this._angularFirestore
      .collection('sections-' + locale)
      .doc(section.id).ref;

    batch.delete(ref);
  }

  private saveImagePart(batch, article, locale) {
    if (!article.articleImageLarge) {
      return;
    }

    let content = this.splitEqually(article.articleImageContent, 3);
    article.articleImageContent = content[0];
    let imagePart = {
      articleImageContent: content[1],
      articleImageContent2: content[2]
    };

    let ref = this._angularFirestore
      .collection('image-parts-' + locale)
      .doc(this.article.id).ref;

    this.cleanModel(imagePart);
    batch.set(ref, imagePart);
  }

  private deleteImagePart(batch, id, locale) {
    return new Promise((resolve, reject) => {
      let ref = this._angularFirestore
        .collection('image-parts-' + locale)
        .doc(id).ref;

      ref.get()
        .then(querySnapshot => {
          if (querySnapshot.exists) {
            batch.delete(ref);
          }
          resolve();
        })
        .catch(error => reject(error));
    });
  }
}
