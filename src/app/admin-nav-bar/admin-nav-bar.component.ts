import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-nav-bar',
  templateUrl: './admin-nav-bar.component.html'
})
export class AdminNavBarComponent implements AfterViewInit {

  currentPath = '';

  constructor(
    private route: ActivatedRoute,
    public authenticationService: AuthenticationService
  ) { 
    this.currentPath = this.route.snapshot.url[0].path;    
  }

  ngAfterViewInit() {

    $('a[routerLink]').each((index, elem) => {
      if ($(elem).attr('routerLink').indexOf(this.currentPath) > -1) {
        $(elem).addClass('active');
      }
    });
  }
}
