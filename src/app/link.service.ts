import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LinkService {

  links: any = {};
  private collectionName = 'links';
  private documentId = 'information';
  private locale;

  constructor(
    private angularFirestore: AngularFirestore
  ) {
    this.locale = window.location.href.indexOf('/pt/') > -1 ? 'pt' : 'en';
  }

  get() {
    return this.angularFirestore
      .collection(this.collectionName + '-' + this.locale)
      .doc(this.documentId)
      .get()
      .pipe(map((querySnapshot: any) => {
        let links = querySnapshot.data();
        if(links){
          this.links = querySnapshot.data();
        }
        return this.links;
      }))
  }

  save(links) {
    return this.angularFirestore
      .collection(this.collectionName + '-' + this.locale)
      .doc(this.documentId)
      .set(links)
  }
}
