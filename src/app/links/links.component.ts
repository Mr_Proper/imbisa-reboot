import { Component, OnDestroy } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { LinkService } from '../link.service';

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html'
})
export class LinksComponent implements OnDestroy {

  private linksSubscription;
  disableSubmit;
  links;

  constructor(
    public linkService: LinkService
  ) {
    this.linksSubscription = this.linkService
      .get()
      .subscribe(links => this.links = links, error => console.log(error));
  }

  ngOnDestroy() {
    if (this.linksSubscription)
      this.linksSubscription.unsubscribe();
  }

  save() {
    this.disableSubmit = true;
    this.linkService.save(this.links)
      .then(() => {
        alert($('#linksSuccessfullySubmitted').text());
        this.linkService.links = this.links;
        this.disableSubmit = false;
      })
      .catch(error => {
        console.log(error);
        this.disableSubmit = false;
      });
  }
}
