import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { PublicArticleService } from './public-article.service';
import { from, forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QueryService {

  searching = true;
  enableInitialSearchWhenNoParameters;
  locales: Array<string>;
  private articleCollectionName = 'articles';
  private publicArticleCollectionName = 'public-articles';
  dateColumnName = 'publishDate';
  private topicColumnName = 'topic';
  private authorColumnName = 'author';
  private regionColumnName = 'region';
  private lastQuery;
  private documents = [];
  private excludeList = [
    'currentIndex',
    'limit',
    'visibility'
  ];

  private localeTopicMappings = [
    ['Amoris Laetitia', 'A Alegria do Amor'],
    ['Catholic Social Teaching', 'Doutrina Social da Igreja'],
    ['Education', 'Educação'],
    ['Events', 'Evento'],
    ['General', 'Geral'],
    ['Health', 'Saúde'],
    ['Interfaith', 'Inter-religioso'],
    ['Joy of the Gospel', 'A Alegria do Evangelho'],
    ['Laudato Si', 'Laudato Sì'],
    ['News', 'Notícias'],
    ['Politics', 'Política'],
    ['Pope Francis', 'Papa Francisco'],
    ['Safeguarding', 'Salvaguarda'],
    ['Xenophobia', 'Xenofobia'],
    ['Youth', 'Juventude']
  ];

  private localeRegionMappings = [
    ['Angola', 'Angola'],
    ['Botswana', 'Botswana'],
    ['Eswatini', 'Eswatini'],
    ['Lesotho', 'Lesotho'],
    ['Mozambique', 'Moçambique'],
    ['Namibia', 'Namíbia'],
    ['Other', 'Outros'],
    ['South Africa', 'África do Sul'],
    ['São Tomé and Príncipe', 'São Tomé e Príncipe'],
    ['Zimbabwe', 'Zimbabwe']
  ];

  constructor(
    private angularFirestore: AngularFirestore,
    private publicArticleService: PublicArticleService
  ) {
    this.lastQuery = this.newQuery();
  }

  get queryEnd() {
    return this.documents.length == this.queryDocuments.length;
  }

  get queryDocuments() {
    let lastIndex = Math.ceil((this.lastQuery.currentIndex + 1) / this.lastQuery.limit) * this.lastQuery.limit - 1;
    return this.documents.filter((document, index) => index <= lastIndex);
  }

  newQuery() {
    return {
      currentIndex: 0,
      limit: 10,
      topic: '',
      author: '',
      region: '',
      startDate: null,
      endDate: null
    };
  }

  private sameQuery(query) {
    let same = true;
    Object.keys(query)
      .filter(key => key !== 'currentIndex')
      .forEach(key => {
        if (same && query[key] !== this.lastQuery[key]) {
          same = false;
        }
      });
    return same;
  }

  private emptyQuery(query) {
    let empty = true;
    Object.keys(query)
      .filter(key => !this.excludeList.includes(key))
      .forEach(key => {
        if (empty && query[key]) {
          empty = false;
        }
      });
    return empty;
  }

  search(query) {
    return new Promise((resolve, reject) => {
      let same = this.sameQuery(query);
      this.lastQuery = Object.assign({}, query);
      if (same) {
        resolve(query);
      } else {
        this.lastQuery.currentIndex = 0;
        this.documents = [];
        this.getDocuments(query, resolve, reject);
      }
    });
  }

  private getDocuments(query, resolve, reject) {
    this.searching = true;
    let promises = [];

    if (!this.locales) {
      let locale = window.location.href.indexOf('/pt/') > -1 ? 'pt' : 'en';
      this.locales = [locale];
    }

    this.locales.forEach(locale => {
      if (query.topic) {
        this.getTopicDocuments(promises, query, locale);
      } else if (query.startDate && query.endDate) {
        this.getDateDocuments(promises, query, locale);
      } else if (query.region) {
        this.getRegionDocuments(promises, query, locale);
      } else if (query.author) {
        this.getAuthorDocuments(promises, query, locale);
      } else if (this.enableInitialSearchWhenNoParameters) {
        this.getFirstPublicDocuments(promises, query, locale);
      }
    });
    Promise.all(promises)
      .then(results => {
        let documents = [].concat.apply([], results);
        documents = documents.sort((a, b) => (a[this.dateColumnName] < b[this.dateColumnName]) ? 1 : -1);
        return this.getImageParts(documents, resolve, reject, query);
      })
      .catch(error => {
        this.searching = false;
        reject(error);
      });
  }

  private searchCallback(results, resolve, query, collectionName, locale) {
    let documents = []
    results.docs.forEach(x => {
      let doc = x.data();
      doc.id = x.id;
      let empty = this.emptyQuery(query);
      doc.visibility = empty ? true : collectionName.indexOf('public') > -1;
      doc.locale = locale;
      documents.push(doc);
    });
    documents = this.filterResults(documents, query);
    resolve(documents);
  }

  private getImageParts(documents, resolve, reject, query) {
    let observables = [];

    documents
      .filter(x => x.articleImageLarge)
      .forEach(document => {
        let observable = this.publicArticleService.getImagePart(document, document.locale);
        observables.push(observable);
      });

    if (observables.length) {
      forkJoin(observables)
        .toPromise()
        .then(() => {
          this.documents = documents;
          this.searching = false;
          resolve(query);
        })
        .catch(error => reject(error));
    } else {

      this.documents = documents;
      this.searching = false;
      resolve(query);
    }
  }

  private filterResults(documents, query) {
    if (query.topic) {
      documents = documents.filter(x => x[this.topicColumnName].indexOf(query.topic) > -1);
    }
    if (query.startDate && query.endDate) {
      documents = documents.filter(x => x[this.dateColumnName] > query.startDate && x[this.dateColumnName] < query.endDate);
    }
    if (query.region) {
      documents = documents.filter(x => x[this.regionColumnName] == query.region);
    }
    if (query.author) {
      documents = documents.filter(x => x[this.authorColumnName].indexOf(query.author) > -1);
    }
    return documents;
  }

  private getFirstPublicDocuments(promises, query, locale) {
    let promise = this.searchFirstPublicDocuments(query, this.publicArticleCollectionName, locale);
    promises.push(promise);
  }

  private getTopicDocuments(promises, query, locale) {
    let promise;
    if (query.visibility.indexOf('Hidden') > -1) {
      promise = this.searchTopic(query, this.articleCollectionName, locale);
      promises.push(promise);
    }
    if (query.visibility.indexOf('Public') > -1) {
      promise = this.searchTopic(query, this.publicArticleCollectionName, locale);
      promises.push(promise);
    }
  }

  private getDateDocuments(promises, query, locale) {
    let promise;
    if (query.visibility.indexOf('Hidden') > -1) {
      promise = this.searchDate(query, this.articleCollectionName, locale);
      promises.push(promise);
    }
    if (query.visibility.indexOf('Public') > -1) {
      promise = this.searchDate(query, this.publicArticleCollectionName, locale);
      promises.push(promise);
    }
  }

  private getRegionDocuments(promises, query, locale) {
    let promise;
    if (query.visibility.indexOf('Hidden') > -1) {
      promise = this.searchRegion(query, this.articleCollectionName, locale);
      promises.push(promise);
    }
    if (query.visibility.indexOf('Public') > -1) {
      promise = this.searchRegion(query, this.publicArticleCollectionName, locale);
      promises.push(promise);
    }
  }

  private getAuthorDocuments(promises, query, locale) {
    let promise;
    if (query.visibility.indexOf('Hidden') > -1) {
      promise = this.searchAuthor(query, this.articleCollectionName, locale);
      promises.push(promise);
    }
    if (query.visibility.indexOf('Public') > -1) {
      promise = this.searchAuthor(query, this.publicArticleCollectionName, locale);
      promises.push(promise);
    }
  }

  private searchFirstPublicDocuments(query, collectionName, locale) {
    return new Promise((resolve, reject) => {
      return this.angularFirestore.collection(collectionName + '-' + locale).ref
        .orderBy(this.dateColumnName, 'desc')
        .limit(10)
        .get()
        .then(results => this.searchCallback(results, resolve, query, collectionName, locale))
        .catch(error => {
          this.searching = false;
          reject(error);
        });
    });
  }

  private searchTopic(query, collectionName, locale) {
    let currentLocale = window.location.href.indexOf('/pt/') > -1 ? 'pt' : 'en';
    let topic = query.topic;
    if (locale !== currentLocale) {
      topic = this.translate(query.topic, this.localeTopicMappings);
    }
    return new Promise((resolve, reject) => {
      this.angularFirestore.collection(collectionName + '-' + locale).ref
        .where(this.topicColumnName, 'array-contains', topic)
        .orderBy(this.dateColumnName, 'desc')
        .get()
        .then(results => this.searchCallback(results, resolve, query, collectionName, locale))
        .catch(error => {
          this.searching = false;
          reject(error);
        });
    });
  }

  private searchAuthor(query, collectionName, locale) {
    return new Promise((resolve, reject) => {
      return this.angularFirestore.collection(collectionName + '-' + locale).ref
        .where(this.authorColumnName, 'array-contains', query.author)
        .orderBy(this.dateColumnName, 'desc')
        .get()
        .then(results => this.searchCallback(results, resolve, query, collectionName, locale))
        .catch(error => {
          this.searching = false;
          reject(error);
        });
    });
  }

  private searchRegion(query, collectionName, locale) {
    let currentLocale = window.location.href.indexOf('/pt/') > -1 ? 'pt' : 'en';
    let region = query.region;
    if (locale !== currentLocale) {
      region = this.translate(query.region, this.localeRegionMappings);
    }
    return new Promise((resolve, reject) => {
      return this.angularFirestore.collection(collectionName + '-' + locale).ref
        .where(this.regionColumnName, '==', region)
        .orderBy(this.dateColumnName, 'desc')
        .get()
        .then(results => this.searchCallback(results, resolve, query, collectionName, locale))
        .catch(error => {
          this.searching = false;
          reject(error);
        });
    });
  }

  private translate(val, list) {
    let translation;
    list.forEach(item => {
      let index = item.indexOf(val);
      if (index > - 1) {
        translation = index === 0 ? item[1] : item[0];
      }
    });
    if (!translation) {
      throw 'Translation not found...';
    }
    return translation;
  }

  private searchDate(query, collectionName, locale) {
    return new Promise((resolve, reject) => {
      return this.angularFirestore.collection(collectionName + '-' + locale).ref
        .where(this.dateColumnName, '>', query.startDate)
        .where(this.dateColumnName, '<', query.endDate)
        .orderBy(this.dateColumnName, 'desc')
        .get()
        .then(results => this.searchCallback(results, resolve, query, collectionName, locale))
        .catch(error => {
          this.searching = false;
          reject(error);
        });
    });
  }

  removeDocument(id) {
    let document = this.documents.find(x => x.id == id);
    if (document) {
      this.documents.splice(this.documents.indexOf(document), 1);
    }
  }
}


