import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { PeopleComponent } from './people/people.component';
import { StoriesComponent } from './stories/stories.component';
import { LoginComponent } from './login/login.component';
import { AuthenticationGuardService } from './authentication-guard.service';
import { ListArticlesComponent } from './list-articles/list-articles.component';
import { ListAdministratorsComponent } from './list-administrators/list-administrators.component';
import { ViewArticleComponent } from './view-article/view-article.component';
import { EditArticleComponent } from './edit-article/edit-article.component';
import { LookupsComponent } from './lookups/lookups.component';
import { ListSubscribersComponent } from './list-subscribers/list-subscribers.component';
import { ConfirmDeactivateService } from './confirm-deactivate.service';
import { EmailTemplateComponent } from './email-template/email-template.component';
import { LinksComponent } from './links/links.component';

const routes: Routes = [
  { path: '', redirectTo: 'home/start', pathMatch: 'full' },
  { path: 'home/:section', component: HomeComponent, runGuardsAndResolvers: 'always' },
  { path: 'contact', component: ContactComponent },
  { path: 'about/:section', component: AboutComponent, runGuardsAndResolvers: 'always' },
  { path: 'people', component: PeopleComponent },
  { path: 'stories', component: StoriesComponent },
  { path: 'articles/:id/edit', component: EditArticleComponent, canDeactivate:[ConfirmDeactivateService] },
  { path: 'articles/:id/edit/:visibility', component: EditArticleComponent, canActivate: [AuthenticationGuardService], canDeactivate:[ConfirmDeactivateService] },
  { path: 'articles/:id/view', component: ViewArticleComponent },
  { path: 'articles', component: ListArticlesComponent, canActivate: [AuthenticationGuardService] },
  { path: 'links', component: LinksComponent, canActivate: [AuthenticationGuardService] },
  { path: 'administrators', component: ListAdministratorsComponent, canActivate: [AuthenticationGuardService] },
  { path: 'subscribers', component: ListSubscribersComponent, canActivate: [AuthenticationGuardService] },
  { path: 'lookups', component: LookupsComponent, canActivate: [AuthenticationGuardService] },
  { path: 'login', component: LoginComponent },
  { path: 'email-template', component: EmailTemplateComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { 
    onSameUrlNavigation: 'reload', 
    scrollPositionRestoration: 'enabled' 
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
