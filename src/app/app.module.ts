import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { PeopleComponent } from './people/people.component';
import { StoriesComponent } from './stories/stories.component';
import { ContactComponent } from './contact/contact.component';
import { FooterComponent } from './footer/footer.component';
import { ImageUploaderComponent } from './image-uploader/image-uploader.component';
import { FileUploadModule } from 'ng2-file-upload';
import { FormTextareaComponent } from './form-textarea/form-textarea.component';
import { ViewArticleComponent } from './view-article/view-article.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { PublicNavBarComponent } from './public-nav-bar/public-nav-bar.component';
import { AdminNavBarComponent } from './admin-nav-bar/admin-nav-bar.component';
import { ListArticlesComponent } from './list-articles/list-articles.component';
import { ListAdministratorsComponent } from './list-administrators/list-administrators.component';
import { EditArticleComponent } from './edit-article/edit-article.component';
import { MailingListComponent } from './mailing-list/mailing-list.component';
import { LookupsComponent } from './lookups/lookups.component';
import { ListLookupComponent } from './list-lookup/list-lookup.component';
import { VisibilityListComponent } from './form-group/visibility-list/visibility-list.component';
import { RegionListComponent } from './form-group/region-list/region-list.component';
import { TopicListComponent } from './form-group/topic-list/topic-list.component';
import { MailingListModalComponent } from './mailing-list-modal/mailing-list-modal.component';
import { ModalDialogModule } from 'ngx-modal-dialog';
import { ListSubscribersComponent } from './list-subscribers/list-subscribers.component';
import { StoriesModalComponent } from './stories-modal/stories-modal.component';
import { EmailTemplateComponent } from './email-template/email-template.component';
import { DocumentLinkComponent } from './form-group/document-link/document-link.component';
import { LinksComponent } from './links/links.component';
import { LoginReguestComponent } from './login-reguest/login-reguest.component';
import { ContentArticleComponent } from './content-article/content-article.component';
import { PublishDateComponent } from './form-group/publish-date/publish-date.component';

@NgModule({
  entryComponents: [
    MailingListModalComponent,
    StoriesModalComponent
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    PeopleComponent,
    StoriesComponent,
    ContactComponent,
    FooterComponent,
    ImageUploaderComponent,
    FormTextareaComponent,
    ViewArticleComponent,
    LoginComponent,
    PublicNavBarComponent,
    AdminNavBarComponent,
    ListArticlesComponent,
    ListAdministratorsComponent,
    EditArticleComponent,
    MailingListComponent,
    LookupsComponent,
    ListLookupComponent,
    VisibilityListComponent,
    RegionListComponent,
    TopicListComponent,
    MailingListModalComponent,
    ListSubscribersComponent,
    StoriesModalComponent,
    EmailTemplateComponent,
    DocumentLinkComponent,
    LinksComponent,
    LoginReguestComponent,
    ContentArticleComponent,
    PublishDateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyDdxXSiiByZ_BzoK86waqdf6WQBmA9WaQw",
      authDomain: "imbisaapp.firebaseapp.com",
      databaseURL: "https://imbisaapp.firebaseio.com",
      projectId: "imbisaapp",
      storageBucket: "imbisaapp.appspot.com",
      messagingSenderId: "651439897430",
      appId: "1:651439897430:web:b6516ffae433cd38"
    }),
    //ScrollToModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    HttpClientModule,
    NgSelectModule,
    ModalDialogModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
