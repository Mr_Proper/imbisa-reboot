import { Component, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { FormControl, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthenticationService } from '../authentication.service';
import { AuthorService } from '../author.service';
import { IArticleService } from '../article-service.interface';
import { PublicArticleService } from '../public-article.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html'
})
export class EditArticleComponent implements OnDestroy, OnInit {

  displayPublishArticleButton = false
  displaySubmitForReviewButton = true;
  private locale;
  private routeSubscription;
  private authorsSubscription;
  private articleSubscription;
  private sectionsSubcription;
  private hasChangesSubscription;
  private visibility = 'Hidden';
  sections = [];
  submitted;
  articleForm;
  disableViewMode;
  viewArticle: any = false;
  disableSubmit;
  articleImageModel;
  new;
  articleService: IArticleService;
  hasChanges;

  constructor(
    private route: ActivatedRoute,
    private _articleService: ArticleService,
    private _publicArticleService: PublicArticleService,
    private angularFirestore: AngularFirestore,
    private router: Router,
    public authenticationService: AuthenticationService,
    public authorService: AuthorService,
    public viewRef: ViewContainerRef
  ) {
    this.locale = window.location.href.indexOf('/pt/') > -1 ? 'pt' : 'en';
    this.getAuthors();
    this.newFormGroup();
  }

  ngOnInit(): void {
    this.routeSubscription = this.route.params
      .subscribe(params => {
        this.visibility = params.visibility ? 'Public' : 'Hidden';
        this.articleService = this.visibility == 'Public' ? this._publicArticleService : this._articleService;
        if (params.id == 'new') {
          this.new = true;
          this.newArticle();
        } else {
          this.getArticle(params.id);
        }
      }, error => console.log(error));
  }

  getAuthors() {
    this.authorsSubscription = this.authorService
      .get()
      .subscribe(() => { }, error => console.log(error));
  }

  getArticle(id) {
    this.articleSubscription = this.articleService
      .getArticle(id, this.locale)
      .subscribe(article => {
        this.setArticle(article);
        this.getSections(id);
      }, error => console.log(error));
  }

  setArticle(article) {
    let copy = Object.assign({}, article);
    this.articleImageModel = {
      id: 'articleImage',
      imageName: copy.articleImageName,
      content: copy.articleImageContent
    };
    this.articleForm.get('authorName').setValue(copy.authorName);
    this.articleForm.get('author').setValue(copy.author);
    this.articleForm.get('region').setValue(copy.region);
    this.articleForm.get('gender').setValue(copy.gender);
    this.articleForm.get('email').setValue(copy.email);
    this.articleForm.get('articleHeadline').setValue(copy.articleHeadline);

    setTimeout(self => {
      if (self.authenticationService.admin) {
        self.articleForm.get('visibility').setValue(self.visibility);
        self.articleForm.get('topic').setValue(copy.topic);
        self.articleForm.get('documentLinkDescription').setValue(copy.documentLinkDescription);
        self.articleForm.get('documentLinkUrl').setValue(copy.documentLinkUrl);
        let publishDate = new DatePipe(navigator.language).transform(new Date(copy.publishDate || copy.timeStamp), 'y-MM-dd');
        self.articleForm.get('publishDate').setValue(publishDate);

        self.displayPublishArticleButton = self.visibility == 'Public';
        self.displaySubmitForReviewButton = !self.displayPublishArticleButton;
      }

      setTimeout(innerSelf => innerSelf.checkHasChanges(), 1000, this);
    }, 1000, this);
  }

  checkHasChanges() {
    if (this.hasChangesSubscription) {
      this.hasChangesSubscription.unsubscribe();
    }
    this.hasChanges = false;
    this.hasChangesSubscription = this.articleForm
      .valueChanges
      .subscribe(() => this.hasChanges = true);
  }

  getSections(id) {
    this.sectionsSubcription = this.articleService
      .getSections(id, this.locale)
      .subscribe(sections => {
        this.sections = [];
        sections.forEach(section => this.addSection(section));
        this.sections.sort((a, b) => a.order - b.order);
      }, error => console.log(error));
  }

  newFormGroup() {
    this.articleForm = new FormGroup({
      authorName: new FormControl('', Validators.required),
      author: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      articleHeadline: new FormControl('', Validators.required)
    });
  }

  newArticle() {
    this.articleService.newArticle();
    this.articleService.newSections();
    this.sections = [];
    this.addSection(null);
    this.submitted = false;
    this.viewArticle = false;
    this.articleImageModel = {
      id: 'articleImage',
      imageName: '',
      content: ''
    };
    setTimeout(self => self.checkHasChanges(), 3000, this);
  }

  addSection(section) {
    if (!section) {
      section = {
        id: this.angularFirestore.createId(),
        bold: false,
        content: '',
        fontSize: '0.9rem',
        imageName: '',
        italic: false,
        order: this.sections.length,
        color: 'dark-grey',
        new: true
      };
    }

    this.sections.splice(section.order, 0, section);
  }

  removeSection(section) {
    this.articleForm.removeControl(section.id);
    this.sections.splice(this.sections.indexOf(section), 1);
  }

  moveSectionUp(section) {
    this.updateImages()
      .then(() => {
        let copy = Object.assign({}, section);
        copy.order--;
        this.removeSection(section);
        this.addSection(copy);
        this.updateSections();
      });
  }

  moveSectionDown(section) {
    this.updateImages()
      .then(() => {
        let copy = Object.assign({}, section);
        copy.order++;
        this.removeSection(section);
        this.addSection(copy);
        this.updateSections();
      });
  }

  ngOnDestroy() {
    if (this.routeSubscription)
      this.routeSubscription.unsubscribe();
    if (this.articleSubscription)
      this.articleSubscription.unsubscribe();
    if (this.sectionsSubcription)
      this.sectionsSubcription.unsubscribe();
    if (this.authorsSubscription)
      this.authorsSubscription.unsubscribe();
    if (this.hasChangesSubscription)
      this.hasChangesSubscription.unsubscribe();
  }

  changeVisibility() {
    this.changeArticleService();
  }

  setGender() {
    setTimeout(self => {
      let selectedAuthors = self.articleForm.get('author').value;
      if (Array.isArray(selectedAuthors)) {
        if (self.locale == 'pt') {
          if (selectedAuthors.indexOf('Sacerdote') > -1 || selectedAuthors.indexOf('Bispo') > -1 || selectedAuthors.indexOf('Masculino') > -1) {
            self.articleForm.get('gender').setValue('Masculino');
            self.articleForm.get('gender').disable();
          } else if (selectedAuthors.indexOf('Fêmea') > -1) {
            self.articleForm.get('gender').setValue('Fêmea');
            self.articleForm.get('gender').disable();
          } else {
            self.articleForm.get('gender').setValue('');
            self.articleForm.get('gender').enable();
          }
        } else {
          if (selectedAuthors.indexOf('Priest') > -1 || selectedAuthors.indexOf('Bishop') > -1 || selectedAuthors.indexOf('Male') > -1) {
            self.articleForm.get('gender').setValue('Male');
            self.articleForm.get('gender').disable();
          } else if (selectedAuthors.indexOf('Female') > -1) {
            self.articleForm.get('gender').setValue('Female');
            self.articleForm.get('gender').disable();
          } else {
            self.articleForm.get('gender').setValue('');
            self.articleForm.get('gender').enable();
          }
        }
      }
    }, 0, this);
  }

  private changeArticleService() {
    this.visibility = this.articleForm.get('visibility').value;

    this.displayPublishArticleButton = this.visibility == 'Public';
    this.displaySubmitForReviewButton = !this.displayPublishArticleButton;

    if (this.visibility == 'Public') {
      this._publicArticleService.article = Object.assign({}, this.articleService.article);
      this._publicArticleService.sections = Object.assign([], this.articleService.sections);
      this.articleService = this._publicArticleService;
    } else {
      this._articleService.article = Object.assign({}, this.articleService.article);
      this._articleService.sections = Object.assign([], this.articleService.sections);
      this.articleService = this._articleService;
    }
  }

  save(valid) {
    this.submitted = true;
    if (valid) {
      this.disableSubmit = true;
      this.updateSections();
      this.updateImages()
        .then(() => this.saveArticle());
    } else {
      alert($('#editArticleInvalidForm').text());
    }
  }

  saveArticle() {
    this.getUpdatedArticle()
      .then(article => {
        let promise;
        if (this.visibility == 'Public') {
          promise = this.savePublicArticle(article, this.sections);
        } else {
          promise = this.saveHiddenArticle(article, this.sections);
        }

        promise
          .then(() => {
            this.articleService.prevArticleId = this.articleService.article.id;
            this.hasChanges = false;
            alert($('#editArticleSuccessfullySubmitted').text());
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;
            this.router.onSameUrlNavigation = 'reload';
            this.router.navigate(['/articles/new/edit' + (this.visibility == 'Public' ? '/public' : '')]);
          })
          .catch(error => {
            console.log(error);
            this.submitted = false;
            this.disableSubmit = false;
          });
      });
  }

  getUpdatedArticle() {
    let self = this;
    return new Promise((resolve, reject) => {
      self.articleImageModel.uploader.updateModel().then(() => {

        let article: any = {
          authorName: self.articleForm.get('authorName').value,
          author: self.articleForm.get('author').value,
          region: self.articleForm.get('region').value,
          gender: self.articleForm.get('gender').value,
          email: self.articleForm.get('email').value,
          articleHeadline: self.articleForm.get('articleHeadline').value,
          articleImageName: self.articleImageModel.imageName,
          articleImageContent: self.articleImageModel.content,
          articleImageLarge: self.articleImageModel.large,
          timeStamp: new Date().toISOString(),
          publishDate: new Date().toISOString(),
          articleHeadlineSearch: self.articleForm.get('articleHeadline').value.toLowerCase().replace(/\s/g, '')
        };
        if (self.authenticationService.admin) {
          article.topic = self.articleForm.get('topic').value;
          article.documentLinkDescription = self.articleForm.get('documentLinkDescription').value;
          article.documentLinkUrl = self.articleForm.get('documentLinkUrl').value;
          article.publishDate = new Date(self.articleForm.get('publishDate').value).toISOString();
        }
        resolve(article);
      });
    });
  }

  updateSections() {
    this.sections
      .filter(section => !section.uploader)
      .forEach((section, index) => {
        section.order = index;
        if (this.articleForm.get(section.id)) {
          section.content = this.articleForm.get(section.id).value;
        }
      });
  }

  updateImages() {
    let promises = this.sections
      .filter(section => section.uploader)
      .map(section => section.uploader.updateModel());
    return Promise.all(promises);
  }

  toggleControl(section) {
    this.articleForm.removeControl(section.id);
    if (section.content.indexOf('data:image') > -1) {
      section.imageName = '';
      section.content = '';
    } else {
      section.content = 'data:image';
    }
  }

  toggleViewMode() {
    if (this.viewArticle !== false) {
      this.viewArticle = false
    } else {
      this.getUpdatedArticle()
        .then(article => {
          this.disableViewMode = true;
          this.updateSections();
          this.updateImages()
            .then(() => {
              this.viewArticle = article;
              this.disableViewMode = false;
            }, error => {
              console.log(error);
              this.disableViewMode = false;
            });
        });
    }
  }

  prevArticle() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate(['/articles/' + this.articleService.prevArticleId + '/edit' + (this.visibility == 'Public' ? '/public' : '')]);
  }

  private savePublicArticle(article, sections) {
    return new Promise((resolve, reject) => {
      let batch = this.angularFirestore.firestore.batch();
      this._publicArticleService._save(batch, article, sections, this.locale);
      this._articleService.deleteArticle(batch, this._publicArticleService.article.id, this.locale)
        .then(() => {
          batch.commit()
            .then(() => resolve())
            .catch(error => reject(error));
        })
        .catch(error => reject(error));
    });
  }

  private saveHiddenArticle(article, sections) {
    return new Promise((resolve, reject) => {
      let batch = this.angularFirestore.firestore.batch();
      this._articleService._save(batch, article, sections, this.locale);
      this._publicArticleService.deleteArticle(batch, this._articleService.article.id, this.locale)
        .then(() => {
          batch.commit()
            .then(() => resolve())
            .catch(error => reject(error));
        })
        .catch(error => reject(error));
    });
  }
}
