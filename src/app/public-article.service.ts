import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ArticleBaseService } from './article-base.service';
import { IArticleService } from './article-service.interface';
import { ArticleService } from './article.service';

@Injectable({
  providedIn: 'root'
})
export class PublicArticleService extends ArticleBaseService implements IArticleService {

  constructor(
    private angularFirestore: AngularFirestore
  ) { 
    super(angularFirestore, 'public-articles');
  }
}
